/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.Subsystem;

public interface Disableable extends Subsystem {
    /**
     * Disables the subsystem.
     */
    void disable();

    /**
     * Enables the subsystem.
     */
    void enable();

    /**
     * Returns a command to disable the subsystem.
     * 
     * @return command to disable the subsystem
     */
    default Command getDisableCommand() {
        return new InstantCommand(this::disable, this);
    }

    /**
     * Returns a command to enable the subsystem.
     * 
     * @return command to enable the subsystem
     */
    default Command getEnableCommand() {
        return new InstantCommand(this::enable, this);
    }
}