/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import frc.robot.RobotMap;

public class IntakeMotor {

    private final VictorSPX m_intakeMotor;
    private final double k_intakeSpeed = 0.7;
    private final double k_outtakeSpeed = -0.4;
    private final double k_shootSpeed = 0.8;
    private MotorSpeedState m_state = MotorSpeedState.STOPPED;

    public enum MotorSpeedState {
        STOPPED,
        INTAKING,
        SHOOTING,
        OUTTAKING
    }

    /**
     * Creates a new Intake subsystem.
     */
    IntakeMotor() {
        m_intakeMotor = new VictorSPX(RobotMap.k_intakeMotorCanId);
    }

    /**
     * Sets the motor speed to a user-supplied value.
     * Uses from -1 to 1 with the PercentOutput control mode.
     * @param state the state to set the motor to.
     */
    public void setSpeed(MotorSpeedState state) {
        double speed = 0;
        switch (state) {
            case INTAKING:
                speed = k_intakeSpeed;
                break;
            case OUTTAKING:
                speed = k_outtakeSpeed;
                break;
            case STOPPED:
                speed = 0;
                break;
            case SHOOTING:
                speed = k_shootSpeed;
                break;
        }
        m_intakeMotor.set(ControlMode.PercentOutput, speed);
        m_state = state;
    }

    public MotorSpeedState getState() {
        return m_state;
    }
}
