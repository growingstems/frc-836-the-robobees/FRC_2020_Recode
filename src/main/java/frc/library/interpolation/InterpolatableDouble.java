/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.interpolation;

/**
 * InterpolatableDouble is a container class for a double value that allows for
 * it to be both linearly interpolated, linearly extrapolated and inverse
 * linearly interpolated.
 */

public class InterpolatableDouble extends InterpolatableValue<InterpolatableDouble, Double>
        implements InverseInterpolatable<InterpolatableDouble> {

    public InterpolatableDouble(double value) {
        super(Double.valueOf(value));
    }

    @Override
    public double linearInverseInterpolate(InterpolatableDouble upper, InterpolatableDouble other) {
        double range = upper.m_value - this.m_value;
        double otherShifted = other.m_value - this.m_value;

        return otherShifted / range;
    }

    @Override
    public InterpolatableDouble linearInterpolate(InterpolatableDouble upper, double x) {
        double range = upper.m_value - this.m_value;

        return new InterpolatableDouble((x * range) + this.m_value);
    }

    @Override
    public InterpolatableDouble linearExtrapolate(InterpolatableDouble upper, double x) {
        /**
         * Extrapolate uses the interpolate method because the interpolate
         * implementation can handle extrapolation for this class.
         */
        return linearInterpolate(upper, x);
    }

    @Override
    public int compareTo(InterpolatableDouble other) {
        if (this.m_value > other.m_value) {
            return 1;
        } else if (this.m_value < other.m_value) {
            return -1;
        } else {
            return 0;
        }
    }

}
