/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package frc.library;

import edu.wpi.first.wpilibj.DriverStation;

import java.lang.Runnable;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.lang.String;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
//import java.util.concurrent.ConcurrentLinkedQueue;

import org.growingstems.frc.util.WpiTimer;

public class Logger implements Runnable {
    private WpiTimer m_timer;
    private boolean m_running;
    private static final double k_loopPeriod_s = 0.002;
    private static final int k_burstSize = 75;
    private static final String k_timestampHeader = "Timestamp_s";
    private double m_lastLogTime_s = 0.0;
    private HashMap<String, Double> m_dataMap;
    private List<String> m_lines = new ArrayList<String>();
    private FileWriter m_log;
    //private ConcurrentLinkedQueue<String> m_queue = new ConcurrentLinkedQueue<String>();

    public Logger(File logFile, List<String> keys) throws IOException {
        m_log = new FileWriter(logFile);

        m_running = true;

        m_dataMap = new HashMap<String, Double>();
        m_dataMap.put(k_timestampHeader, 0.0);
        for (var key : keys) {
            m_dataMap.put(key, 0.0);
        }

        //generate header row
        String str = "";
        boolean first = true;
        for (var key : m_dataMap.entrySet()) {
            if (!first) {
                str += ", ";
            }
            str += key.getKey().toString();
            first = false;
        }
        m_lines.add(str);

        m_timer = new WpiTimer();
        m_timer.start();
    }

    public void stopLogging() {
        m_running = false;
    }

    @Override
    public void run() {
        //try {
            //while (m_running) {
                //var now_s = m_timer.get();
                ////DriverStation.reportError("running: now_s: " + now_s + "\n", true);
                ////DriverStation.reportError("lastLogTime_s: " + m_lastLogTime_s + "\n", true);
                //if (now_s > m_lastLogTime_s + k_loopPeriod_s) {
                    ////copy data from queue
                    //String[] rows = new String[1];
                    //m_queue.toArray(rows);

                    //for (var row : rows) {
                        //m_lines.add(row);
                    //}

                    ////check if write needs to happen
                    //if (m_lines.size() > k_burstSize) {
                        //writeFile();
                    //}

                    //m_lastLogTime_s = now_s;
                //}
            //}
            //m_log.close();
        //}
        //catch (IOException e) {
            //System.err.println(e.getMessage());
            //DriverStation.reportError("\n\n\n" + e.getMessage() + "\n\n\n", true);
        //}
    }

    public boolean keyExists(String key) {
        return m_dataMap.containsKey(key);
    }
    public void setDataPoint(String key, double val) {
        m_dataMap.put(key, val);
    }
    public void grabLine() {
        var now = m_timer.get();
        m_dataMap.put(k_timestampHeader, now);

        String str = "";
        boolean first = true;
        for (var val : m_dataMap.entrySet()) {
            if (!first) {
                str += ", ";
            }
            str += val.getValue().toString();
            first = false;
        }

        //m_queue.add(str);
        m_lines.add(str);

        if (m_lines.size() > k_burstSize) {
            try {
                writeFile();
            }
            catch (IOException e) {
                DriverStation.reportError("\n\n\n" + e.getMessage() + "\n\n\n", true);
            }
        }
    }

    private void writeFile() throws IOException {
        for (var line : m_lines) {
            m_log.write(line + "\n");
        }
        m_log.flush();
        m_lines.clear();
    }
}
