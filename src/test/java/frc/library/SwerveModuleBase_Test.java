package frc.library;

import static org.junit.Assert.assertEquals;

import org.growingstems.math.Angle;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class SwerveModuleBase_Test {
    private static final Vector2d m_position = new Vector2d(0.0, 0.0);

    @Test
    @Parameters({
        "0.0, 0.0, 0.0, 0.0, Default test",
        "1.0, 0.0, 1.0, 0.0, Drive wheel power 1.0 test",
        "0.5, 0.0, 0.5, 0.0, Drive wheel power 0.5 test",
        "2.0, 0.0, 1.0, 0.0, Drive wheel power positive limit test",
        "-1.0, 0.0, 0.0, 0.0, Drive wheel power negative rejection test 1",
        "-0.5, 0.0, 0.0, 0.0, Drive wheel power negative rejection test 2",
        "-2.0, 0.0, 0.0, 0.0, Drive wheel power negative rejection test 3",
        "0.0, 1.0, 0.0, 0.0, Neutral Deadband test",
        "1.0, 180.0, -1.0, 0.0, Reverse optimization test"
    })
    public void setOpenLoopTest(double power, double steerAngle_deg, double expectedDrivePower, double expectedSteerPosition, String testName) {
        SwerveModuleBase_Concrete module = new SwerveModuleBase_Concrete(m_position, 1.0, 1.0);

        module.setOpenLoop(power, new Angle(steerAngle_deg, AngleUnit.DEGREES));
        assertEquals("(" + testName + ") - Check sent drive power.", expectedDrivePower, module.getDriveOpenLoop_argPower(), 1.0E-6);
        assertEquals("(" + testName + ") - Check sent steer position.", expectedSteerPosition, Math.toDegrees(module.getSteerPositionClosedLoop_argPosition()), 1.0E-6);
    }

    @Test
    @Parameters({
        "0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Default test",
        "0.0, 5.0, 0.0, 0.0, 5.0, 0.0, Forward test",
        "0.0, -5.0, 0.0, 0.0, -5.0, 0.0, Reverse test",
        "0.0, 5.0, 0.0, 90.0, 0.0, 5.0, Turn after test",
        "0.0, 5.0, 90.0, 0.0, 5.0, 0.0, Turned before test",
        "2.0, 10.0, 0.0, -90.0, 0.0, -8.0, Turn and move test"
    })
    public void getForwardKinematicsTest(double prevPos, double currPos, double prevSteer, double currSteer, double expectedX, double expectedY, String testName) {
        SwerveModuleBase_Concrete module = new SwerveModuleBase_Concrete(m_position, 1.0, 1.0);

        // Load previous state
        module.setDrivePosition_return(prevPos);
        module.setSteerAngle_return(Math.toRadians(prevSteer));
        module.getForwardKinematics();

        // Load current state
        module.setDrivePosition_return(currPos);
        module.setSteerAngle_return(Math.toRadians(currSteer));
        Vector2d result = module.getForwardKinematics();

        assertEquals("(" + testName + ") - X.", expectedX, result.getX(), 1.0E-9);
        assertEquals("(" + testName + ") - Y.", expectedY, result.getY(), 1.0E-9);
    }

    public void setOpenLoopSimulationTest() {
        SwerveModuleBase_Concrete module = new SwerveModuleBase_Concrete(m_position, 1.0, 1.0);

        module.setOpenLoop(1.0, new Angle(89.0, AngleUnit.DEGREES)); // Rotate ~90 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(1.0, new Angle(178.0, AngleUnit.DEGREES)); // Rotate ~90 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(1.0, new Angle(-93.0, AngleUnit.DEGREES)); // Rotate ~90 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(1.0, new Angle(-4.0, AngleUnit.DEGREES)); // Rotate ~90 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(1.0, new Angle(0.0, AngleUnit.DEGREES)); // Align back to 0.0 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(0.0, new Angle()); // Resets power to 0.0, angle is now 360.0

        assertEquals("Check sent drive power.", 0.0, module.getDriveOpenLoop_argPower(), 1.0E-6);
        assertEquals("Check sent steer position.", 360.0, Math.toDegrees(module.getSteerPositionClosedLoop_argPosition()), 1.0E-6);

        module.setOpenLoop(1.0, new Angle(89.0, AngleUnit.DEGREES)); // Rotate ~90 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(1.0, new Angle(178.0, AngleUnit.DEGREES)); // Rotate ~90 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(1.0, new Angle(-93.0, AngleUnit.DEGREES)); // Rotate ~90 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(1.0, new Angle(-4.0, AngleUnit.DEGREES)); // Rotate ~90 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(1.0, new Angle(0.0, AngleUnit.DEGREES)); // Align back to 0.0 degrees
        module.setSteerAngle_return(module.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module.setOpenLoop(0.0, new Angle()); // Resets power to 0.0, angle is now 720.0

        assertEquals("Check sent drive power.", 0.0, module.getDriveOpenLoop_argPower(), 1.0E-6);
        assertEquals("Check sent steer position.", 720.0, Math.toDegrees(module.getSteerPositionClosedLoop_argPosition()), 1.0E-6);
    }

    @Test
    public void setOpenLoopSimReverseOptimizeTest() {
        SwerveModuleBase_Concrete module1 = new SwerveModuleBase_Concrete(m_position, 1.0, 1.0);

        module1.setOpenLoop(1.0, new Angle(180.0, AngleUnit.DEGREES)); // Rotate 90 degrees
        module1.setSteerAngle_return(module1.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        assertEquals("Check sent drive power.", -1.0, module1.getDriveOpenLoop_argPower(), 1.0E-6);
        module1.setOpenLoop(1.0, new Angle(0.0, AngleUnit.DEGREES)); // Rotate 90 degrees
        module1.setSteerAngle_return(module1.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        assertEquals("Check sent drive power.", 1.0, module1.getDriveOpenLoop_argPower(), 1.0E-6);
        module1.setOpenLoop(1.0, new Angle(180.0, AngleUnit.DEGREES)); // Rotate 90 degrees
        module1.setSteerAngle_return(module1.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position

        assertEquals("Check sent drive power.", -1.0, module1.getDriveOpenLoop_argPower(), 1.0E-6);
        assertEquals("Check sent steer position.", 0.0, Math.toDegrees(module1.getSteerPositionClosedLoop_argPosition()), 1.0E-6);

        SwerveModuleBase_Concrete module2 = new SwerveModuleBase_Concrete(m_position, 1.0, 1.0);

        module2.setOpenLoop(1.0, new Angle(91.0, AngleUnit.DEGREES)); // Rotate 90 degrees
        module2.setSteerAngle_return(module2.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position
        module2.setOpenLoop(1.0, new Angle(-90.0, AngleUnit.DEGREES)); // Rotate 90 degrees
        module2.setSteerAngle_return(module2.getSteerPositionClosedLoop_argPosition()); // Simulate the wheel going to position

        assertEquals("Check sent drive power.", 1.0, module2.getDriveOpenLoop_argPower(), 1.0E-6);
        assertEquals("Check sent steer position.", -90.0, Math.toDegrees(module2.getSteerPositionClosedLoop_argPosition()), 1.0E-6);
    }
}

class SwerveModuleBase_Concrete extends SwerveModuleBase {
    private double m_setDriveOpenLoop_argPower = 0.0;
    private double m_setDriveVelocityClosedLoop_argVelocity = 0.0;
    private double m_setDrivePositionClosedLoop_argPosition = 0.0;
    private double m_setSteerOpenLoop_argPower = 0.0;
    private double m_setSteerPositionClosedLoop_argPosition = 0.0;

    private double m_getSteerAngle_return = 0.0;
    private double m_getSteerVelocity_return = 0.0;
    private double m_getDrivePosition_return = 0.0;
    private double m_getDriveVelocity_return = 0.0;

    public SwerveModuleBase_Concrete(Vector2d modulePos, double driveInchesPerSensorUnit, double steerRadiansPerSensorUni) {
        super(modulePos, driveInchesPerSensorUnit, steerRadiansPerSensorUni);
    }

    public double getDriveOpenLoop_argPower() {
        return m_setDriveOpenLoop_argPower;
    }

    public double getDriveVelocityClosedLoop_argVelocity() {
        return m_setDriveVelocityClosedLoop_argVelocity;
    }

    public double getDrivePositionClosedLoop_argPosition() {
        return m_setDrivePositionClosedLoop_argPosition;
    }

    public double getSteerOpenLoop_argPower() {
        return m_setSteerOpenLoop_argPower;
    }

    public double getSteerPositionClosedLoop_argPosition() {
        return m_setSteerPositionClosedLoop_argPosition;
    }

    public void setSteerAngle_return(double returnValue) {
        m_getSteerAngle_return = returnValue;
    }

    public void setSteerVelocity_return(double returnValue) {
        m_getSteerVelocity_return = returnValue;
    }

    public void setDrivePosition_return(double returnValue) {
        m_getDrivePosition_return = returnValue;
    }

    public void setDriveVelocity_return(double returnValue) {
        m_getDriveVelocity_return = returnValue;
    }

    @Override
    protected boolean setDriveOpenLoop(double power) {
        m_setDriveOpenLoop_argPower = power;
        return true;
    }

    @Override
    protected boolean setDriveVelocityClosedLoop(double velocity) {
        m_setDriveVelocityClosedLoop_argVelocity = velocity;
        return true;
    }

    @Override
    protected boolean setDrivePositionClosedLoop(double position) {
        m_setDrivePositionClosedLoop_argPosition = position;
        return true;
    }

    @Override
    protected boolean setSteerOpenLoop(double power) {
        m_setDrivePositionClosedLoop_argPosition = power;
        return true;
    }

    @Override
    protected boolean setSteerPositionClosedLoop(double position) {
        m_setSteerPositionClosedLoop_argPosition = position;
        return true;
    }

    @Override
    protected double getRawSteerAngle_su() {
        return m_getSteerAngle_return;
    }

    @Override
    protected double getRawSteerVelocity_sups() {
        return m_getSteerVelocity_return;
    }

    @Override
    protected double getRawDrivePosition_su() {
        return m_getDrivePosition_return;
    }

    @Override
    protected double getRawDriveVelocity_sups() {
        return m_getDriveVelocity_return;
    }

    @Override
    protected void velocityVectorControl(Vector2d velocityVector_inps) {
         //TODO: Add content
    }
}
