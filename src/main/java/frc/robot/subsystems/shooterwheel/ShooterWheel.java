/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.shooterwheel;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.library.Disableable;
import frc.library.interpolation.InterpolatableDouble;
import frc.library.interpolation.InterpolatingTreeMap;
import frc.robot.FieldDimensions;
import frc.robot.RobotState;

import java.util.function.DoubleSupplier;

public class ShooterWheel extends SubsystemBase implements Disableable {

    private final TalonSRX m_motorMaster;
    private final VictorSPX m_motorFollower;
    private final RobotState m_robotState;

    private double m_speedGoal_RPM;

    private final int k_masterCanId = 21;
    private final int k_followerCanId = 22;

    private final double k_speedP = 0.13;
    private final double k_speedI = 0.0;
    private final double k_speedD = 0.0001;
    private final double k_speedF = 0.024;
    private final int k_speedSlot = 0;

    private final int k_standbySpeed_RPM = 4000;
    private final int k_idleSpeed_RPM = 2000;
    private final int k_allowableSpeedError_RPM = 200;

    public static final double k_muzzleVelocity_inps = 270.0;
    private final double k_zeroPower = 0.0391007;
    private final double k_velocityCompensationFactor = 6.2;
    private final double k_sensorUnitPerRpm = 6.826666666; // 4096/600

    private static final InterpolatingTreeMap<InterpolatableDouble, InterpolatableDouble> k_lookupTableOuter = new InterpolatingTreeMap<>();
    private static final double[][] k_lookupTableOuterArray = {
        {  81.78, 6100.0 },
        { 100.00, 6100.0 },
        { 118.00, 4300.0 },
        { 130.50, 4300.0 },
        { 171.75, 4300.0 },
        { 185.00, 4330.0 },
        { 208.53, 4230.0 },
        { 215.0,  4510.0 },
        { 230.0,  4630.0 },
        { 252.0,  4675.0 },
    };

    static {
        for (int i = 0; i < k_lookupTableOuterArray.length; i++) {
            InterpolatableDouble key = new InterpolatableDouble(k_lookupTableOuterArray[i][0]);
            InterpolatableDouble value = new InterpolatableDouble(k_lookupTableOuterArray[i][1]);
            k_lookupTableOuter.put(key, value);
        }
    };
/*
    private static final InterpolatingTreeMap<InterpolatableDouble, InterpolatableDouble> k_lookupTableInner = new InterpolatingTreeMap<>();
    private static final double[][] k_lookupTableInnerArray = {
        { 91.295,  5250.0 },
        { 98.6,    5200.0 },
        { 102.665, 5050.0 },
        { 105.86,  4820.0 },
        { 109.565, 4570.0 },
        { 115.685, 4410.0 },
        { 122.49,  4280.0 },
        { 133.88,  4180.0 },
        { 145.84,  4140.0 },
        { 151.6,   4060.0 },
        { 157.99,  4080.0 },
        { 170.16,  4090.0 },
        { 185.2,   4100.0 },
        { 203.0,   4100.0 },
        { 220.0,   4200.0 },
        { 222.03,  4320.0 }
    };

    static {
        for (int i = 0; i < k_lookupTableInnerArray.length; i++) {
            InterpolatableDouble key = new InterpolatableDouble(k_lookupTableInnerArray[i][0]);
            InterpolatableDouble value = new InterpolatableDouble(k_lookupTableInnerArray[i][1]);
            k_lookupTableInner.put(key, value);
        }
    };
*/
    /**
     * The various states that the Shooter Wheel can be in.
     */
    public static enum States {
        STOPPED,
        IDLE,
        IDLE_OVERRIDE,
        STANDBY,
        STANDBY_OVERRIDE,
        SHOOT,
        SHOOT_OVERRIDE,
        CUSTOM_SPEED,
        DISABLED
    };

    private boolean m_shouldShoot = false;
    private States m_shooterState = States.STOPPED;

    /**
     * Creates a new ShooterWheel subsystem.
     *
     * @param robotState a reference to the actively used RobotState object.
     */
    public ShooterWheel(RobotState robotState) {
        SmartDashboard.setDefaultNumber("RPM Offset", 0.0);
        m_motorMaster = new TalonSRX(k_masterCanId);
        m_motorFollower = new VictorSPX(k_followerCanId);
        m_robotState = robotState;

        m_motorFollower.follow(m_motorMaster);

        // TODO: Remove factory default when CTRE Utils is used
        // Reset to Factory Default
        m_motorMaster.configFactoryDefault(100);
        m_motorFollower.configFactoryDefault(100);

        // Set speed controller direction
        m_motorMaster.setInverted(InvertType.None);
        m_motorFollower.setInverted(InvertType.OpposeMaster);

        // Set PIDF
        m_motorMaster.config_kP(k_speedSlot, k_speedP, 100);
        m_motorMaster.config_kI(k_speedSlot, k_speedI, 100);
        m_motorMaster.config_kD(k_speedSlot, k_speedD, 100);
        m_motorMaster.config_kF(k_speedSlot, k_speedF, 100);

        // Setup sensor
        m_motorMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 100);
        m_motorMaster.setSensorPhase(true);

        // Setup Voltage Compensation and Peak Output
        m_motorMaster.configVoltageCompSaturation(12.0, 100);
        m_motorMaster.enableVoltageCompensation(true);
        m_motorMaster.configPeakOutputForward(1.0, 100);
        m_motorMaster.configPeakOutputReverse(0.0, 100);

        m_motorMaster.setNeutralMode(NeutralMode.Coast);

        // Set output to disabled
        m_motorMaster.set(ControlMode.Disabled, 0.0);

        // This will prevent follower mode.
        // m_motorFollower.set(ControlMode.Disabled, 0.0);
        // My guess is that using set at all pulls the controller out of follow mode.
        // m_motorFollower.set(ControlMode.PercentOutput, 0.0);
    }

    @Override
    public void periodic() {
        if (getState() == States.DISABLED) {
            return;
        }

        // Update auto mode if the Shooter Wheel is currently in a automatic state.
        if (isAutoMode()) {
            updateAutomaticMode();
        }

        // Only update the Shooter Wheel's shoot speed if we are actually trying to
        // shoot.
        if (getState() == States.SHOOT || getState() == States.SHOOT_OVERRIDE) {
            updateShootSpeed();
        }
    }

    private static double calculateRpm(double distance, FieldDimensions.GoalType gPref) {
        InterpolatableDouble rpm = new InterpolatableDouble(0.0);

        switch(gPref) {
            case INNER:
                //rpm = k_lookupTableInner.getValue(new InterpolatableDouble(distance), false);
                break;
            case OUTER:
                rpm = k_lookupTableOuter.getValue(new InterpolatableDouble(distance), false);
                break;
        }

        var rpmVal = rpm.getValue();
        SmartDashboard.putNumber("Base RPM", rpmVal);
        rpmVal += SmartDashboard.getNumber("RPM Offset", 0.0);
        SmartDashboard.putNumber("Adj RPM", rpmVal);
        return rpmVal;
    }

    /**
     * Use to determine if the shooter is ready to make a shot on the goal or not.
     *
     * @return whether or not the Shooter Wheel can make the shot.
     */
    public boolean isReadyToShoot() {
        if (getState() == States.SHOOT || getState() == States.SHOOT_OVERRIDE) {
            return Math.abs(m_speedGoal_RPM - getSpeed_RPM()) < k_allowableSpeedError_RPM;
        } else {
            return false;
        }
    }

    private boolean isAutoMode() {
        return getState() == States.SHOOT || getState() == States.STANDBY || getState() == States.IDLE;
    }

    private void setSpeed(double speed) {
        m_speedGoal_RPM = speed;
        SmartDashboard.putNumber("Set Shoot Speed", m_speedGoal_RPM);
        m_speedGoal_RPM = Math.min(m_speedGoal_RPM, 6100);
        double sensorUnits = speed * k_sensorUnitPerRpm;
        m_motorMaster.set(ControlMode.Velocity, sensorUnits);
    }

    public double getSpeed_RPM() {
        double sensorUnits = m_motorMaster.getSelectedSensorVelocity();
        return sensorUnits / k_sensorUnitPerRpm;
    }

    private void setAutomaticMode() {
        // Puts the Shooter Wheel into the appropriate automatic state.
        updateAutomaticMode();
    }

    private void setStopMode() {
        setState(States.STOPPED);
        m_motorMaster.set(ControlMode.Disabled, 0.0);
    }

    private void setIdleMode(boolean override) {
        if (override) {
            setState(States.IDLE_OVERRIDE);
        } else {
            setState(States.IDLE);
        }

        setSpeed(k_idleSpeed_RPM);
    }

    private void setStandbyMode(boolean override) {
        if (override) {
            setState(States.STANDBY_OVERRIDE);
        } else {
            setState(States.STANDBY);
        }

        setSpeed(k_standbySpeed_RPM);
    }

    private void setShootMode(boolean override) {
        if (override) {
            setState(States.SHOOT_OVERRIDE);
        } else {
            setState(States.SHOOT);
        }
    }

    private void setCustomSpeedMode(double speed_RPM) {
        setState(States.CUSTOM_SPEED);
        setSpeed(speed_RPM);
    }

    private void updateShootSpeed() {
        double shotDistance = m_robotState.getCompensatedShotVector().magnitude();
        SmartDashboard.putNumber("Shot Distance", shotDistance);
        double speed = calculateRpm(shotDistance, m_robotState.getGoalPreference());
        setSpeed(speed);
    }

    private void updateAutomaticMode() {
        if (m_robotState.isInShootZone()) {
            /*
            if (m_shouldShoot) {
                setShootMode(false);
            } else {
                setStandbyMode(false);
            }
            */
            setShootMode(false);
        } else {
            setIdleMode(false);
        }
    }

    public States getState() {
        return m_shooterState;
    }

    /**
     * Gets a command that is used to tell the Shooter Wheel to attempt a shot at
     * the goal.
     * <pre>
     * This command is only useful in Automatic mode. Use getSetAutomaticCommand()
     * to set the Shooter Wheel into Automatic mode.
     * </pre>
     * <b>NOTE</b>: Use getSetShootCommand() if you wish to override the Shooter
     * Wheel into shoot mode.
     *
     * @return a fire command.
     */
    public Command getFireCommand() {
        return new InstantCommand(() -> m_shouldShoot = true, this);
    }

    /**
     * Gets a command that is used to tell the Shooter Wheel not to attempt a shot.
     * <pre>
     * This command is only useful in Automatic mode. Use getSetAutomaticCommand()
     * to set the Shooter Wheel into Automatic mode.
     * </pre>
     *
     * @return a cease fire command.
     */
    public Command getCeaseFireCommand() {
        return new InstantCommand(() -> m_shouldShoot = false, this);
    }

    /**
     * Gets a command that sets the Shooter Wheel into Automatic mode. This mode is
     * used to automatically decide what speed the Shooter Wheel should be in. First
     * time this command is ran, the Shooter Wheel will be set to Cease Fire.
     * <pre>
     * Use getFireCommand() and getCeaseFireCommand() to command the Shooter Wheel
     * to attempt to fire or not.
     * </pre>
     *
     * @return a command that sets the Shooter Wheel to Idle mode.
     */
    public Command getSetAutomaticCommand() {
        return new FunctionalCommand(
            () -> {},
            () -> setAutomaticMode(),
            (unused) -> setStopMode(),
            () -> false,
            this);
    }

    /**
     * Gets a command that sets the speed of Shooter Wheel to a custom speed.
     *
     * @param speed_RPM the speed the Shooter Wheel's goal will set to.
     * @return a command that sets the Shooter Wheel's speed.
     */
    public Command getSetSpeedCommand(DoubleSupplier speed_RPM) {
        return new FunctionalCommand(
            () -> {},
            () -> setCustomSpeedMode(speed_RPM.getAsDouble()),
            (unused) -> setStopMode(),
            () -> false,
            this);
    }

    /**
     * Gets a command that overrides the Shooter Wheel into Idle mode. This mode is
     * used to idle the Shooter Wheel in order to conserve power.
     *
     * @return a command that sets the Shooter Wheel to Idle mode.
     */
    public Command getSetIdleCommand() {
        return new FunctionalCommand(
            () -> setIdleMode(true),
            () -> {},
            (unused) -> setStopMode(),
            () -> false,
            this);
    }

    /**
     * Gets a command that overrides the Shooter Wheel into Standby mode. This mode
     * is used to ready the Shooter Wheel for a shot.
     *
     * @return a command that sets the Shooter Wheel to Standby mode.
     */
    public Command getSetStandbyCommand() {
        return new FunctionalCommand(
            () -> setStandbyMode(true),
            () -> {},
            (unused) -> setStopMode(),
            () -> false,
            this);
    }

    /**
     * Gets a command that overrides the Shooter Wheel into Shoot mode. The Shooter
     * Wheel speed gets updated each cycle of periodic in this mode.
     *
     * @return a command that sets the Shooter Wheel to Shoot mode.
     */
    public Command getSetShootCommand() {
        return new FunctionalCommand(
            () -> setShootMode(true),
            () -> {},
            (unused) -> setStopMode(),
            () -> false,
            this);
    }

    /**
     * Stops the shooter wheel and sets the state to disabled.
     */
    @Override
    public void disable() {
        setStopMode();
        setState(States.DISABLED);
    }

    /**
     * Enables the shooter wheel in a stop state. <br>
     * <br>
     * No-ops if the shooter wheel is not disabled.
     */
    @Override
    public void enable() {
        //Does not call setState because setState does nothing if the robot is disabled.
        if (m_shooterState == States.DISABLED) {
            m_shooterState = States.STOPPED;
        }
    }

    private void setState(States state) {
        if (m_shooterState == States.DISABLED) {
            return;
        }

        m_shooterState = state;
    }
}
