/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.drive;

import java.util.ArrayList;
import java.util.function.Supplier;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.TalonFXInvertType;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.sensors.CANCoder;
import com.ctre.phoenix.sensors.SensorInitializationStrategy;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Angle;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.util.timer.TimerI;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.RobotMap;
import frc.robot.RobotState;
import frc.library.PidController;
import frc.library.SwerveDrive;
import frc.library.SwerveModuleFalconDrvFalconStr;

public class Drive extends SubsystemBase {
    private SwerveDrive<?> m_swerveDrive;
    private RobotState m_robotState;

    private final Vector2d k_modulePosFrontRight = new Vector2d(10.125, -10.125);
    private final Vector2d k_modulePosFrontLeft = new Vector2d(10.125, 10.125);
    private final Vector2d k_modulePosBackLeft = new Vector2d(-10.125, 10.125);
    private final Vector2d k_modulePosBackRight = new Vector2d(-10.125, -10.125);

    private static double m_steerP = 1.0;
    private static double m_steerI = 0.005;
    private static double m_steerD = 15.0;
    // TODO: Do the math to calculate F gain like LV code does.
    private static double m_steerF = 0.366225481;

    private static double m_unloadedSteerP = 0.1;
    private static double m_unloadedSteerI = 0.0;
    private static double m_unloadedSteerD = 0.0;
    // TODO: Do the math to calculate F gain like LV code does.
    private static double m_unloadedSteerF = 0.366225481;

    // TODO: tune these gains
    private static double k_headingP = 0.02;
    private static double k_headingI = 0;
    private static double k_headingD = 0.001;

    private static double k_steerEncoderOffsetFR = 338.643  * -1.0;
    private static double k_steerEncoderOffsetFL = 175.781  * -1.0;
    private static double k_steerEncoderOffsetBL = 285.117  * -1.0;
    private static double k_steerEncoderOffsetBR = 245.3027 * -1.0;

    private static double k_pBotSteerEncoderOffsetFR = 146.074 * -1.0;
    private static double k_pBotSteerEncoderOffsetFL = 218.232 * -1.0;
    private static double k_pBotSteerEncoderOffsetBL = 264.990 * -1.0;
    private static double k_pBotSteerEncoderOffsetBR = 2.549   * -1.0;

    private static final double m_moduleDriveFR_ipsu = 362.005 / 366819.0;
    private static final double m_moduleDriveFL_ipsu = 362.005 / 365948.0;
    private static final double m_moduleDriveBL_ipsu = 362.005 / 365414.0;
    private static final double m_moduleDriveBR_ipsu = 362.005 / 366498.0;
    private static final double m_moduleSteer_rpsu = (2.0 * Math.PI) / 4096.0;

    private static final double k_driveStableGain = 0.002;

    private final SwerveModuleFalconDrvFalconStr m_frontRight = new SwerveModuleFalconDrvFalconStr(
        new TalonFX(RobotMap.k_swerveFrontRightPowerCanId),
        new TalonFX(RobotMap.k_swerveFrontRightSteerCanId),
        new CANCoder(RobotMap.k_swerveFrontRightCanCoderCanId),
        k_modulePosFrontRight,
        m_moduleDriveFR_ipsu,
        m_moduleSteer_rpsu
    );
    private final SwerveModuleFalconDrvFalconStr m_frontLeft = new SwerveModuleFalconDrvFalconStr(
        new TalonFX(RobotMap.k_swerveFrontLeftPowerCanId),
        new TalonFX(RobotMap.k_swerveFrontLeftSteerCanId),
        new CANCoder(RobotMap.k_swerveFrontLeftCanCoderCanId),
        k_modulePosFrontLeft,
        m_moduleDriveFL_ipsu,
        m_moduleSteer_rpsu
    );
    private final SwerveModuleFalconDrvFalconStr m_backLeft = new SwerveModuleFalconDrvFalconStr(
        new TalonFX(RobotMap.k_swerveBackLeftPowerCanId),
        new TalonFX(RobotMap.k_swerveBackLeftSteerCanId),
        new CANCoder(RobotMap.k_swerveBackLeftCanCoderCanId),
        k_modulePosBackLeft,
        m_moduleDriveBL_ipsu,
        m_moduleSteer_rpsu
    );
    private final SwerveModuleFalconDrvFalconStr m_backRight = new SwerveModuleFalconDrvFalconStr(
        new TalonFX(RobotMap.k_swerveBackRightPowerCanId),
        new TalonFX(RobotMap.k_swerveBackRightSteerCanId),
        new CANCoder(RobotMap.k_swerveBackRightCanCoderCanId),
        k_modulePosBackRight,
        m_moduleDriveBR_ipsu,
        m_moduleSteer_rpsu
    );

    /**
     * Creates a new Drive system.
     *
     * @param robotState a reference to the actively used RobotState object.
     */
    public Drive(RobotState robotState) {
        m_robotState = robotState;

        ArrayList<SwerveModuleFalconDrvFalconStr> baseSwerveModules = new ArrayList<>();

        baseSwerveModules.add(m_frontRight);
        baseSwerveModules.add(m_frontLeft);
        baseSwerveModules.add(m_backLeft);
        baseSwerveModules.add(m_backRight);

        m_frontRight.getSteerEncoder().configMagnetOffset(k_steerEncoderOffsetFR, 100);
        m_frontLeft.getSteerEncoder().configMagnetOffset(k_steerEncoderOffsetFL, 100);
        m_backLeft.getSteerEncoder().configMagnetOffset(k_steerEncoderOffsetBL, 100);
        m_backRight.getSteerEncoder().configMagnetOffset(k_steerEncoderOffsetBR, 100);

        m_frontRight.getDriveController().setInverted(TalonFXInvertType.Clockwise);
        m_frontLeft.getDriveController().setInverted(TalonFXInvertType.CounterClockwise);
        m_backLeft.getDriveController().setInverted(TalonFXInvertType.Clockwise);
        m_backRight.getDriveController().setInverted(TalonFXInvertType.CounterClockwise);

        m_frontRight.getDriveController().setSensorPhase(false);
        m_frontLeft.getDriveController().setSensorPhase(true);
        m_backLeft.getDriveController().setSensorPhase(false);
        m_backRight.getDriveController().setSensorPhase(false);

        for (SwerveModuleFalconDrvFalconStr module : baseSwerveModules) {
            module.getSteerController().configFactoryDefault(100);

            module.getDriveController().setNeutralMode(NeutralMode.Brake);
            module.getDriveController().configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 100);

            module.getSteerController().config_kP(0, m_steerP);
            module.getSteerController().config_kI(0, m_steerI);
            module.getSteerController().config_kD(0, m_steerD);
            module.getSteerController().config_kF(0, m_steerF);
            module.getSteerController().configAllowableClosedloopError(0, 15, 100);
            module.getSteerController().config_IntegralZone(0, 50, 100);

            module.getSteerController().config_kP(1, m_unloadedSteerP);
            module.getSteerController().config_kI(1, m_unloadedSteerI);
            module.getSteerController().config_kD(1, m_unloadedSteerD);
            module.getSteerController().config_kF(1, m_unloadedSteerF);
            module.getSteerController().configAllowableClosedloopError(1, 15, 100);
            module.getSteerController().config_IntegralZone(1, 50, 100);

            module.getSteerController().configMotionCruiseVelocity(2654, 100);
            module.getSteerController().configMotionAcceleration(15922, 100);

            module.getSteerController().configNeutralDeadband(0.001, 100);
            module.getSteerController().configRemoteFeedbackFilter(module.getSteerEncoder(), 0);
            module.getSteerController().configSelectedFeedbackSensor(TalonFXFeedbackDevice.RemoteSensor0, 0, 100);
            module.getSteerController().setSensorPhase(true);
            module.getSteerController().setInverted(TalonFXInvertType.Clockwise);
            module.getSteerController().configVoltageCompSaturation(10.0, 100);
            module.getSteerController().enableVoltageCompensation(true);
            module.getSteerController().setNeutralMode(NeutralMode.Brake);

            module.getSteerEncoder().configSensorInitializationStrategy(SensorInitializationStrategy.BootToAbsolutePosition);
        }

        m_swerveDrive = new SwerveDrive<>(baseSwerveModules);
    }

    /**
     * Gets the difference in position since the last time this function was called.
     * This delta in position is robot centric where +X is forwards relative to the
     * module and +Y is left relative to the module. The first time this function is
     * called, it always returns (0.0, 0.0).
     *
     * @return the difference in position since the last time this function was called.
     */
    public Vector2d getForwardKinematics() {
        return m_swerveDrive.getForwardKinematics();
    }

    /**
     * Gets the drive's current velocity as a vector relative to the robot.
     *
     * @return Curent velocity of the drive
     */
    public Vector2d getVelocityVector() {
        return m_swerveDrive.getVelocityVector();
    }

    private void manualDriveWithFoc(Vector2d translation, double rotation) {
        Vector2d result = translation.rotate(m_robotState.getRobotHeading().inverse());
        SmartDashboard.putNumber("ReS_x", result.getX());
        SmartDashboard.putNumber("ReS_y", result.getY());
        SmartDashboard.putNumber("ReS_t", rotation);
        m_swerveDrive.setOpenLoop(result, rotation);
    }

    public Command getDirectDriveCommand(Supplier<Double> x, Supplier<Double> y, Supplier<Double> rotation) {
        return new FunctionalCommand(
            () -> {},
            () -> manualDriveWithFoc(new Vector2d(x.get(), y.get()), rotation.get()),
            (unused) -> m_swerveDrive.stop(),
            () -> false,
            this);
    }
    public Command getDirectDriveCommand(Supplier<Vector2d> velocity, Supplier<Double> rotation) {
        return new FunctionalCommand(
            () -> {},
            () -> {
                var velVec = velocity.get();
                manualDriveWithFoc(new Vector2d(velVec.getX(), velVec.getY()), rotation.get());
            },
            (unused) -> m_swerveDrive.stop(),
            () -> false,
            this);
    }


    public Command getVelocityAndHeadingCommand(Supplier<Vector2d> velocity, Supplier<Angle> heading, Supplier<Boolean> isFinished) {
        return getVelocityAndHeadingCommand(() -> {
            Pose2d pose = new Pose2d();
            pose.setPosition(velocity.get());
            pose.setRotation(heading.get());
            return pose;
        }, isFinished);
    }

    public Command getTeleopNoTurningCommand(Supplier<Vector2d> velocity, Supplier<Angle> angle) {
        return getVelocityAndHeadingCommand(() -> new Pose2d(velocity.get(), angle.get()), () -> false);
    }

    public Command getVelocityAndHeadingCommand(Supplier<Pose2d> velocityAndHeading, Supplier<Boolean> isFinished) {
        PidController pidController = new PidController(k_headingP, k_headingI, k_headingD);

        TimerI timer = new WpiTimer();
        return new FunctionalCommand(
            timer::start,
            () -> {
                var control = velocityAndHeading.get();
                var goalVelocity = control.getPosition();
                var currentHeading = m_robotState.getLatestRobotPose().getRotation();
                var goalHeading = control.getRotation();
                double difference = currentHeading.difference(goalHeading).getValue(AngleUnit.DEGREES);
                double pidOutput = pidController.update(difference);
                SmartDashboard.putNumber("Auto time", timer.get());
                SmartDashboard.putNumber("GoalVel X", goalVelocity.getX());
                SmartDashboard.putNumber("GoalVel Y", goalVelocity.getY());
                SmartDashboard.putNumber("GoalVel Dir", goalVelocity.getAngle().getValue(AngleUnit.DEGREES));
                SmartDashboard.putNumber("GoalHeading", goalHeading.getValue(AngleUnit.DEGREES));
                SmartDashboard.putNumber("Turn PID Input", difference);
                SmartDashboard.putNumber("Turn PID Output", pidOutput);
                double commandedMagnitude = Math.min(goalVelocity.magnitude(), 1.0);
                var robotVelocity_inps = m_robotState.getRobotVelocityVector();
                var perpendicularVelocity_inps = goalVelocity.scale(robotVelocity_inps.dot(goalVelocity) / goalVelocity.magnitude()).subtractSet(robotVelocity_inps);
                goalVelocity.addSet(perpendicularVelocity_inps.scaleSet(k_driveStableGain * commandedMagnitude));
                manualDriveWithFoc(goalVelocity, pidOutput);
            },
            (unused) -> m_swerveDrive.stop(),
            isFinished::get,
            this);
    }
}
