/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.sensors.CANCoder;

import org.growingstems.math.Vector2d;

/**
 * A Swerve Module that is made up of two FalconFXs to control the wheel power
 * and steer direction and a single CANCoder to measure the steer angle.<br>
 * <br>
 * All devices passed in on construction should be left as is in terms of their
 * Feedback Coefficient. CANCoder's configFeedbackCoefficient() and TalonFx's
 * configSelectedFeedbackCoefficient() should either be set to 1.0 or left as is
 * after calling configFactoryDefault().
 */
public class SwerveModuleFalconDrvFalconStr extends SwerveModuleBase {

    private final TalonFX m_motorDrive;
    private final TalonFX m_motorSteer;
    private final CANCoder m_steerSensor;

    /**
     * Creates a Swerve Module that is made up of two FalconFXs to control the wheel
     * power and steer direction and a single CANCoder to measure the steer
     * angle.<br>
     * <br>
     * All devices passed in on construction should be left as is in terms of their
     * Feedback Coefficient. CANCoder's configFeedbackCoefficient() and TalonFx's
     * configSelectedFeedbackCoefficient() should either be set to 1.0 or left as is
     * after calling configFactoryDefault().
     *
     * @param motorDrive                The TalonFX used to control the drive wheel
     *                                  power.
     * @param motorSteer                The TalonFX used to control the steering of
     *                                  the drive wheel.
     * @param steerSensor               The CANCoder used to sense the steer angle
     *                                  of the drive wheel.
     * @param modulePosition            The relative position of module on the
     *                                  robot's chassis.
     * @param driveInchesPerSensorUnit  The conversion factor to be used to convert
     *                                  the drive wheels position from Inches to raw
     *                                  Sensor Units.
     * @param steerRadiansPerSensorUnit The conversion factor to be used to convert
     *                                  the drive wheels steer angle position from
     *                                  Radians to raw Sensor Units.
     */
    public SwerveModuleFalconDrvFalconStr(TalonFX motorDrive, TalonFX motorSteer, CANCoder steerSensor,
            Vector2d modulePosition, double driveInchesPerSensorUnit, double steerRadiansPerSensorUnit) {
        super(modulePosition, driveInchesPerSensorUnit, steerRadiansPerSensorUnit);

        m_motorDrive = motorDrive;
        m_motorSteer = motorSteer;
        m_steerSensor = steerSensor;
    }

    public TalonFX getDriveController() {
        return m_motorDrive;
    }

    public TalonFX getSteerController() {
        return m_motorSteer;
    }

    public CANCoder getSteerEncoder() {
        return m_steerSensor;
    }

    @Override
    protected boolean setDriveOpenLoop(double power) {
        m_motorDrive.set(TalonFXControlMode.PercentOutput, power);
        return true;
    }

    @Override
    protected boolean setDriveVelocityClosedLoop(double velocity) {
        m_motorDrive.set(TalonFXControlMode.Velocity, velocity);
        return true;
    }

    @Override
    protected boolean setDrivePositionClosedLoop(double position) {
        m_motorDrive.set(TalonFXControlMode.MotionMagic, position);
        return true;
    }

    @Override
    protected boolean setSteerOpenLoop(double power) {
        m_motorSteer.set(TalonFXControlMode.PercentOutput, power);
        return true;
    }

    @Override
    protected boolean setSteerPositionClosedLoop(double position) {
        m_motorSteer.set(TalonFXControlMode.MotionMagic, position);
        return true;
    }

    @Override
    protected double getRawSteerAngle_su() {
        return m_motorSteer.getSelectedSensorPosition();
    }

    @Override
    protected double getRawSteerVelocity_sups() {
        // Convert from SU per 100ms to SU per 1s
        return m_steerSensor.getVelocity() * 10.0;
    }

    @Override
    protected double getRawDrivePosition_su() {
        return m_motorDrive.getSelectedSensorPosition();
    }

    @Override
    protected double getRawDriveVelocity_sups() {
        // Convert from SU per 100ms to SU per 1s
        return m_motorDrive.getSelectedSensorVelocity() * 10.0;
    }

    @Override
    protected void velocityVectorControl(Vector2d velocityVector_inps) {
      //TODO: Add content
    }
}
