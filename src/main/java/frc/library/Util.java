/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

/**
 * Utilities contains various utility methods.
 */
public final class Util {
    /**
     * Coerces a value to be between a lower and upper bound inclusively.
     *
     * @param value     Value to be coerced.
     * @param lowBound  Lower bounds.
     * @param highBound Upper bounds.
     * @return Coerced version of value. [lowBound, highBound]
     */
    public static double coerceValue(double value, double lowBound, double highBound) {
        if (value < lowBound) {
            return lowBound;
        } else if (value > highBound) {
            return highBound;
        } else {
            return value;
        }
    }

    public static double applyDeadzone(double joystickInput, double deadZone) {
        if (Math.abs(joystickInput) < deadZone) {
            //in deadzone, do nothing
            return 0.0;
        }

        double newVal = (Math.abs(joystickInput) - deadZone) / (1.0 - deadZone);
        return (joystickInput < 0.0 ? -newVal : newVal);
    }

    /**
     * Shifts a bounded angle to the correct scope of a unbounded angle. Used to
     * shift a bounded angle such that it will be the shortest distance to the
     * reference angle. An example of use is with a Swerve Module that is kept at a
     * steer angle that is continuous from (-inf, inf), and is being driven by an
     * angle from [-Pi, Pi).
     *
     * @param wrappedAngle   Driving angle that is bounded from [-Pi, Pi).
     * @param referenceAngle Angle that the new angle's scope is based off of.
     * @return Wound up angle that is within the scope of the reference angle.
     */
    public static double shiftWrappedAngleScope(final double wrappedAngle, final double referenceAngle) {
        return shiftWrappedAngleScope(wrappedAngle, referenceAngle, -Math.PI, Math.PI);
    }

    /**
     * Shifts a bounded angle to the correct scope of a unbounded angle. Used to
     * shift a bounded angle such that it will be the shortest distance to the
     * reference angle. An example of use is with a Swerve Module that is kept at a
     * steer angle that is continuous from (-inf, inf), and is being driven by an
     * angle from [-wrappedLow, wrappedHigh).
     *
     * @param wrappedAngle   Driving angle that is bounded from [-wrappedLow,
     *                       wrappedHigh).
     * @param referenceAngle Angle that the new angle's scope is based off of.
     * @param wrappedLow     The lower bound of wrappedAngle.
     * @param wrappedHigh    The upper bound of wrappedAngle.
     * @return Wound up angle that is within the scope of the reference angle.
     */
    public static double shiftWrappedAngleScope(final double wrappedAngle, final double referenceAngle,
            final double wrappedLow, final double wrappedHigh) {
        final double wrappedRange = wrappedHigh - wrappedLow;

        // First offset the angle to simplify math.
        final double adjustedRef = referenceAngle - wrappedLow;
        final double referenceScope = Math.floor(adjustedRef / (2 * Math.PI)) * (2 * Math.PI);

        final double shiftedAngle = wrappedAngle + referenceScope;
        final double delta = referenceAngle - shiftedAngle;

        if (delta <= wrappedRange / 2 && delta >= -wrappedRange / 2) {
            return shiftedAngle;
        } else if (delta > wrappedRange / 2) {
            return shiftedAngle + wrappedRange;
        } else {
            return shiftedAngle - wrappedRange;
        }
    }
}
