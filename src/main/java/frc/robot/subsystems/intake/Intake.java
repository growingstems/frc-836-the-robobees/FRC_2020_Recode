/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;

import frc.library.Disableable;
import frc.library.ScheduledStateMachine;
import frc.robot.subsystems.intake.IntakeMotor.MotorSpeedState;

import java.util.function.BooleanSupplier;

import org.growingstems.util.statemachine.ExecState;
import org.growingstems.util.statemachine.InitState;
import org.growingstems.util.statemachine.StaticTransition;


/**
 * Aquires power cells from field. <br>
 * <br>
 * Controls the pnumatics that retracts and extends the intake. <br>
 * Controls a motor that spins the wheel on the intake. <br>
 * Power cells are immediately deposited into the Hopper subsystem.
 */
public class Intake implements Disableable{
    /** The motor for the intake. */
    private final IntakeMotor motor = new IntakeMotor();
    /** The pnumatic actuator for the intake. */
    private final IntakeActuator actuator = new IntakeActuator();

    private static final double k_pistonStowTime_s = 0.5;
    private static final double k_pistonDeployTime_s = 0.5;

    private final BooleanSupplier m_isShooting;

    private boolean m_doStowed = true;
    private IntakeMotor.MotorSpeedState m_intakeSpeed = MotorSpeedState.STOPPED;

    private final Timer m_transitionTimer = new Timer();

    private final ScheduledStateMachine m_stateMachine;

    /**
     * Creates a new Intake subsystem.
     *
     * @param isShooting a supplier to get whether the robot should be shooting or not.
     */
    public Intake(BooleanSupplier isShooting) {
        m_isShooting = isShooting;

        m_transitionTimer.start();
        var stowedState = new ExecState("Stowed", this::stowed);
        var deployedState = new ExecState("Deployed", this::deployed);
        var stowingState = new InitState("Stowing", this::stowingInit);
        var deployingState = new InitState("Deploying", this::deployingInit);

        stowingState.addTransition(new StaticTransition(stowedState,
            () -> m_transitionTimer.hasElapsed(k_pistonStowTime_s)));
        deployingState.addTransition(new StaticTransition(deployedState,
            () -> m_transitionTimer.hasElapsed(k_pistonDeployTime_s)));

        stowedState.addTransition(new StaticTransition(deployingState, () -> !m_doStowed));
        stowingState.addTransition(new StaticTransition(deployingState, () -> !m_doStowed));
        deployedState.addTransition(new StaticTransition(stowingState, () -> m_doStowed));
        deployingState.addTransition(new StaticTransition(stowingState, () -> m_doStowed));

        m_stateMachine = new ScheduledStateMachine(stowedState);
        m_stateMachine.start();
    }

    private void deployingInit() {
        m_transitionTimer.reset();
        actuator.deploy();
        motor.setSpeed(MotorSpeedState.STOPPED);
    }

    private void stowingInit() {
        m_transitionTimer.reset();
        actuator.stow();
        motor.setSpeed(MotorSpeedState.INTAKING);
    }

    private void stowed() {
        motor.setSpeed(m_isShooting.getAsBoolean() ? MotorSpeedState.SHOOTING : MotorSpeedState.STOPPED);
    }

    private void deployed() {
        if (m_intakeSpeed.equals(MotorSpeedState.SHOOTING)) {
            System.err.println("m_intakespeed set to SHOOTING in deployed state.");
            return;
        }

        motor.setSpeed(m_intakeSpeed);
    }

    /**
     * Stows the intake.
     *
     * @return {@link Command} to stow the intake
     */
    public Command getStowCommand() {
        return new InstantCommand(()->m_doStowed = true, this);
    }

    /**
     * Deploys the intake while stopping the motor.
     *
     * @return {@link Command} to deploy the intake into an Idle state
     */
    public Command getDeployIdleCommand() {
        return new InstantCommand(()->{
            m_doStowed = false;
            m_intakeSpeed = MotorSpeedState.STOPPED;
        }, this);
    }

    /**
     * Deploys the intake while intaking the motor.
     *
     * @return {@link Command} to deploy the intake into an Intaking state
     */
    public Command getDeployIntakeCommand() {
        return new InstantCommand(()->{
            m_doStowed = false;
            m_intakeSpeed = MotorSpeedState.INTAKING;
        }, this);
    }

    /**
     * Deploys the intake while outtaking the motor.
     *
     * @return {@link Command} to deploy the intake into an Outtaking state
     */
    public Command getDeployOuttakeCommand() {
        return new InstantCommand(()->{
            m_doStowed = false;
            m_intakeSpeed = MotorSpeedState.OUTTAKING;
        }, this);
    }

    /**
     * Stops the state machine, then sets the intake to stowed and stopped.
     */
    @Override
    public void disable() {
        m_stateMachine.stop();
        m_doStowed = true;
        m_intakeSpeed = MotorSpeedState.STOPPED;
        actuator.stow();
        motor.setSpeed(m_intakeSpeed);
    }

    /**
     * Enables the intake in a stopped and stowed state. <br>
     * <br>
     * No-ops if the intake is already running.
     */
    @Override
    public void enable() {
        if (m_stateMachine.isRunning()) {
            return;
        }
        disable();
        m_stateMachine.start();
    }
}
