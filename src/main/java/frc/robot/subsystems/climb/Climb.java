/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.climb;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Climb extends SubsystemBase {

    /**
     * Creates a new Intake subsystem.
     */
    public Climb() {

    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
    }
}
