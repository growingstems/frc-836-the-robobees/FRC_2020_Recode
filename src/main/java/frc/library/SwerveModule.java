/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import org.growingstems.math.Angle;
import org.growingstems.math.Vector2d;

/**
 * Interface used for a swerve module at its most fundamental level. A swerve
 * module is a physical module on a robot that contains a single wheel that can
 * be both powered to create lateral movement along the ground and steered to
 * control the direction of the lateral motion.
 */
public interface SwerveModule {

    /**
     * Sets the conversion factor for both the drive wheel's position and steer
     * angle position.
     *
     * @param driveInchesPerSensorUnit  Conversion factor for the drive wheel in
     *                                  Inches/SU.
     * @param steerRadiansPerSensorUnit Conversion factor for the steer angle of the
     *                                  module in Radians/SU.
     */
    public abstract void setConversionFactors(double driveInchesPerSensorUnit, double steerRadiansPerSensorUnit);

    /**
     * Sets the wheel's velocity and direction.<br>
     * <br>
     * A steer angle of 0.0 is pointing forward in the +X direction, positive values
     * are counter-clockwise.
     *
     * @param velocity_fps Drive wheel velocity in feet/second. [0.0, inf)
     * @param steerAngle   Steer goal. [-Pi, Pi) radians
     */
    public abstract void setVelocityClosedLoop(double velocity_fps, Angle steerAngle);

    /**
     * Sets the wheel's power and direction.<br>
     * <br>
     * A steer angle of 0.0 is pointing forward in the +X direction, positive values
     * are counter-clockwise.
     *
     * @param power      Drive wheel power. [0.0, 1.0]
     * @param steerAngle Steer goal. [-Pi, Pi) radians.
     */
    public abstract void setOpenLoop(double power, Angle steerAngle);

    /**
     * Sets both steer and drive components of the submodule into open loop control
     * mode. Generally should only be used for testing purposes as using this could
     * burn holes in the field's carpet and create flat spots on the submodule's
     * wheel.
     *
     * @param drivePower Drive wheel power. [0.0, 1.0]
     * @param steerPower Wheel steer power. [0.0, 1.0]
     */
    public abstract void setTotalOpenLoop(double drivePower, double steerPower);

    /**
     * Returns the position of the module relative to robot origin.
     *
     * @return Module position relative to robot origin.
     */
    public abstract Vector2d getModuleLocation();

    /**
     * Gets the difference in position since the last time this function was called.
     * This delta in position is module centric where +X is forwards relative to the
     * module and +Y is left relative to the module. The first time this function is
     * called, it always returns (0.0, 0.0).
     *
     * @return the difference in position since the last time this function was called.
     */
    public abstract Vector2d getForwardKinematics();

   /**
     * Gets the drive's current velocity as a vector relative to the robot.
     *
     * @return Curent velocity of the drive
     */
    public abstract Vector2d getVelocityVector();
}
