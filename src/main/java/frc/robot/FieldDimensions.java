/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Vector2d;

/**
 * The FieldDimensions class is used to store all of the dimensional data of the
 * field related to this project's relevant year (2020 - Infinite Recharge).
 */
public final class FieldDimensions {

    // Field Dimensions
    public static final double k_fieldWidth_in = 15.0 * 12.0;
    public static final double k_fieldLength_in = 30.0 * 12.0;

    // Power Port Dimensions
    public static final double k_powerPortTargetWidth_in = 39.25;
    public static final double k_powerPortTargetHeightFromGround_in = 81.25;
    public static final double k_powerPortTargetHeight_in = 17.0;
    public static final double k_powerPortTargetTopFromGround_in = 98.25;
    public static final double k_powerPortFaceTo_innerGoalFace_in = 29.5;
    public static final double k_heightFromFloorToHalfofLoadingBayTarget_in = 16.5;
    public static final double k_powerPortHeight_in = 30.0;
    public static final double k_powerPortWidth_in = 34.641;

    // Feeder Station Dimensions
    //public static final double k_loadingBayTargetWidth_in = 7.0;
    //public static final double k_loadingBayTargetHeight_in = 11.0;
    //public static final double k_loadingBayTargetHeightFromGround_in = 11.0;

    // Power Cell Locations
    public static final double k_powerCellDiameter_in = 7.0;

    // Trench Dimensions

    // Inner Cone
    public static final double k_innerConeDistance_in = 220.0;
    public static final Angle k_innerConeFov = new Angle(25.0, AngleUnit.DEGREES);
    public static final Angle k_innerConeOrientation = new Angle(Math.PI, AngleUnit.RADIANS);

    // Outer Cone
    public static final double k_outerConeDistance_in = 320.0;
    public static final Angle k_outerConeFov = new Angle(120.0, AngleUnit.DEGREES);
    public static final Angle k_outerConeOrientation = new Angle(Math.PI, AngleUnit.DEGREES);

    // Auto Cone
    public static final double k_autoShootConeDistance_in = 260.0;
    public static final Angle k_autoShootConeFov = new Angle(25.0, AngleUnit.DEGREES);
    public static final Angle k_autoShootConeOrientation = new Angle(Math.PI, AngleUnit.DEGREES);

    // Targeting Cone
    public static final double k_targetVisibleConeDistance_in = 300.0;
    public static final Angle k_targetVisibleConeFov = new Angle(200.0, AngleUnit.DEGREES);
    public static final Angle k_targetVisibleConeOrientation = new Angle(Math.PI, AngleUnit.RADIANS);

    // Positon Overides
    public static final Vector2d k_positionOverrideUp_in = new Vector2d(-139.5, -66.965);
    public static final Vector2d k_positionOverrideDown_in = new Vector2d(-314.625, 0.0);
    public static final Vector2d k_positionOverrideLeft_in = new Vector2d(-185.0, 80.8125);
    public static final Vector2d k_positionOverrideRight_in = new Vector2d(-242.63, -133.875);

    // Power Port Corners
    public static final Vector2d k_powerPortOuterCenter_in = new Vector2d(k_fieldLength_in + 29.25, k_fieldWidth_in / 2.0);
    public static final double k_powerPortOuterHeight_in = 98.25;
    public static final double k_powerPortInnerHeight_in = 98.25;
    public static final Vector2d k_powerPortInnerCenter_in = new Vector2d(29.25, -66.965);
    // TODO: Replace with 3D versions with rotation when the Pose3D class is made.
    //private final Pose3D k_powerPortOuterCenter = new Pose3D(0, -66.965, 98.25, 180)
    //private final Pose3D k_powerPortInnerCenter = new Pose3D(29.25, -66.965, 98.25, 180);;
    //private final Pose3D k_powerPortOpposingOuterCenter = new Pose3D(323.25, -66.965, 98.25, 180);
    //private final Pose3D k_powerOpposingPortOuterCenter = new Pose3D(352.5, -66.965, 98.25, 180);

    public static enum GoalType {
        INNER,
        OUTER
    };
}
