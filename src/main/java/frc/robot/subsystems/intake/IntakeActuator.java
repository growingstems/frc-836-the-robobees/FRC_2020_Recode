/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake;

import edu.wpi.first.wpilibj.Solenoid;
import frc.robot.RobotMap;

public class IntakeActuator {
    private final Solenoid m_intakeActuator;

    /**
     * Creates a new Intake subsystem.
     */
    IntakeActuator() {
        m_intakeActuator = new Solenoid(RobotMap.k_pcmCanId, RobotMap.k_intakeSolenoid);
    }

    /**
     * Stows the intake.
     */
    public void stow() {
        m_intakeActuator.set(false);
    }

    /**
     * Deploys the intake.
     */
    public void deploy() {
        m_intakeActuator.set(true);
    }
}
