/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.turret;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.RobotState;

import java.util.function.Supplier;

import org.growingstems.math.Angle;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;

public class Turret extends SubsystemBase {

    // Turret Hardware Limits/Parameters
    public static final int k_absoluteEncoderOffset_su = -163;
    /** Calculated using 16358 sensor units per semicircle */
    public static final double k_sensorUnitPerRadian = 5206.9131181944;
    public static final double k_aimSoftLimitLeft_rad = Math.toRadians(40.0);
    public static final double k_aimSoftLimitRight_rad = Math.toRadians(-180.0);
    public static final double k_allowableAngleError_rad = Math.toRadians(8.0);
    public static final Vector2d k_turretCenterToRobotOrigin_in = new Vector2d(-7.748, 0);

    // Motion Magic Closed Loop Settings
    public static final double k_motionMagicP = 0.8;
    public static final double k_motionMagicI = 0.005;
    public static final double k_motionMagicD = 20.0;
    public static final double k_motionMagicF = 0.0;
    public static final int k_turretMotionMagicIZone = 60;

    // Motion Magic Parameters
    public static final double k_turretMaxVelocity = 6666.0;
    public static final int k_turretMotionMagicVelocity = (int) (k_turretMaxVelocity * 0.85);
    public static final int k_turretMotionMagicAcceleration = (int) (k_turretMaxVelocity * 4.0);

    private final Supplier<Angle> m_angleSupplier;
    private final Supplier<Angle> m_angleGoalSupplier;

    public static enum TurretState {
        STOPPED,
        NEUTRAL,
        AUTO,
        MANUAL,
        DISABLED
    };

    private TurretState m_state;
    private final RobotState m_robotState;

    /**
     * Creates a new Turret subsystem.
     *
     * @param robotState a reference to the actively used RobotState object.
     * @param angleSupplier Angle of the turret in raw sensor units.
     * @param angleGoalSupplier Current goal of the angle closed-loop controller.
     */
    public Turret(RobotState robotState, Supplier<Angle> angleSupplier, Supplier<Angle> angleGoalSupplier) {
        m_robotState = robotState;
        m_angleSupplier = angleSupplier;
        m_angleGoalSupplier = angleGoalSupplier;
    }

    /**
     * Gets the current state of the turret.
     *
     * @return the current turret state.
     */
    public TurretState getState() {
        return m_state;
    }

    /**
     * Gets the current position of the turret relative to the robot.
     *
     * @return the current position of the turret as an Angle.
     */
    public Angle getAngle() {
        return m_angleSupplier.get();
    }

    /**
     * Gets the current position goal of the turret relative to the robot. This is
     * the goal that the closed-loop controller is trying to achieve.
     *
     * @return the current position goal of the turret as an Angle.
     */
    public Angle getAngleGoal() {
        return m_angleGoalSupplier.get();
    }

    /**
     * This method converts a sensor unit value, originating from the turret
     * encoder, into radians. This value is bounded from -Pi to Pi.
     *
     * @param sensorUnits Value originating from the turret encoder.
     * @return Converted value in radians. [-Pi, Pi)
     */
    protected Angle sensorUnitsToAngle(double sensorUnits) {
        double processRadians = sensorUnits / k_sensorUnitPerRadian;

        if (processRadians < -Math.PI) {
            processRadians += 2.0 * Math.PI;
        }

        return new Angle(processRadians, Angle.AngleUnit.RADIANS);
    }

    /**
     * Getter method for the center of the turret relative to the robot's origin.
     *
     * @return Position of the turret's center relative to the robot's origin.
     */
    public Vector2d getTurretCenterToRobotOrigin() {
        return k_turretCenterToRobotOrigin_in;
    }

    /**
     * Calculates the center of the turret relative to the field. Exclusively used
     * by RobotState which then caches this location for all subsystems to use.
     *
     * @param robotPose The robot's current pose in inches and radians.
     * @return The position of the turret's center relative to the field's origin.
     */
    public static Vector2d getCenterOfTurret(Pose2d robotPose) {
        Vector2d turretTranslationVector = k_turretCenterToRobotOrigin_in;

        turretTranslationVector = turretTranslationVector.rotate(robotPose.getRotation());

        return robotPose.getPosition().add(turretTranslationVector);
    }

    public boolean isReadyToShoot() {
        return Math.abs(getAngleGoal().subtract(getAngle()).getValue(AngleUnit.RADIANS)) < k_allowableAngleError_rad;
    }

    /**
     * Calculates turret's angular position based on its current shot vector, then
     * converted into sensor units used for direct robot implementation.
     *
     * @return The rotation of the turret in sensor units.
     */
    public Angle getCalculatedPosition() {
        return m_robotState.getShotVector().getAngle();
    }

    private void setTurretStop() {
        m_state = TurretState.STOPPED;
    }

    private void setTurretNeutral() {
        m_state = TurretState.NEUTRAL;
    }

    private void setTurretAuto() {
        m_state = TurretState.AUTO;
    }

    private void setTurretManual() {
        m_state = TurretState.MANUAL;
    }

    public Command getStopCommand() {
        return new InstantCommand(() -> setTurretStop(), this);
    }

    public Command getNeutralCommand() {
        return new InstantCommand(() -> setTurretNeutral(), this);
    }

    public Command getAutoCommand() {
        return new InstantCommand(() -> setTurretAuto(), this);
    }

    public Command getManualCommand() {
        return new InstantCommand(() -> setTurretManual(), this);
    }
}
