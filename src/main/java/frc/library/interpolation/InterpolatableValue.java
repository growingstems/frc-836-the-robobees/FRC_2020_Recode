/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.interpolation;

/**
 * InterpolatableValue is a generic container class to be used for both linearly
 * interpolating and linearly extrapolating a generic type 'T'.
 * <p>
 * 'B' is used to pass the base class into Interpolatable.
 */
public abstract class InterpolatableValue<B, T> implements Interpolatable<B>, Comparable<B> {

    protected T m_value;

    /**
     * Constructor for InterpolatableValue. Stores the value to be used for
     * interpolation.
     *
     * @param value Value to be stored as an interpolatable.
     */
    public InterpolatableValue(T value) {
        m_value = value;
    }

    /**
     * Getter method for the interpolatable value.
     *
     * @return The stored interpolatable value.
     */
    public T getValue() {
        return m_value;
    }

    /**
     * Setter method for the interpolatable value.
     *
     * @param value Value to be stored as an interpolatable value.
     */
    public void setValue(T value) {
        m_value = value;
    }

}
