package frc.robot.subsystems.limelight;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.FieldDimensions;

import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Vector2d;

public class Limelight {
    private final NetworkTable m_networkTable = NetworkTableInstance.getDefault().getTable("limelight");
    private final NetworkTableEntry m_validTarget = m_networkTable.getEntry("tv");
    private final NetworkTableEntry m_verticalTargetAngle = m_networkTable.getEntry("tx");
    private final NetworkTableEntry m_horizontalTargetAngle = m_networkTable.getEntry("ty");
    private final NetworkTableEntry m_area = m_networkTable.getEntry("ta");
    private final Angle m_pitch;
    private final double m_height_in;

    public Limelight(Angle pitch, double height_in) {
        m_pitch = pitch;
        m_height_in = height_in;
    }

    public Vector2d getTargetVector_in() {
        //System.out.println("this is a test message");
        if(getValidTarget()){
            var angleToTarget = getAngleToTargetHorizontal();
            //if (angleToTarget.getValue(AngleUnit.DEGREES))
            return new Vector2d(getDistanceToTarget_in(), angleToTarget);
        }
        return null;
    }

    public boolean getValidTarget() {
        return m_validTarget.getDouble(0.0) == 1.0;
    }

    public Angle getAngleToTargetHorizontal() {
        var angle_deg = m_horizontalTargetAngle.getDouble(0.0);
        SmartDashboard.putNumber("LLHorizontal", angle_deg);
        return new Angle(-angle_deg, AngleUnit.DEGREES);
    }

    public Angle getAngleToTargetVertical() {
        var angle_deg = m_verticalTargetAngle.getDouble(0.0);
        SmartDashboard.putNumber("LLVertical", angle_deg);
        return new Angle(angle_deg, AngleUnit.DEGREES);
    }
    public double getTargetArea() {
        return m_area.getDouble(0.0);
    }

    public double getDistanceToTarget_in() {
        double height_in = FieldDimensions.k_powerPortTargetHeightFromGround_in - m_height_in +
                           FieldDimensions.k_powerPortTargetHeight_in / 2.0;
        Angle angleFromGround = m_pitch.subtract(getAngleToTargetVertical());
        return height_in / angleFromGround.tan();
    }
}
