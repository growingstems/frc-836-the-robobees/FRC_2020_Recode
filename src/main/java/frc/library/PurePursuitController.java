/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import frc.library.PathParser.Point;
import java.util.List;

import org.growingstems.math.Angle;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * A tool to follow a given path by using the next points to determine speed and direction. <br>
 * <br>
 * Pure Pursuit uses the closest point to the robot to determine the speed the robot should drive.
 * The look-ahead point is used to determine the direction the robot should drive.
 */
public class PurePursuitController {
    private static final int k_maxLookAheadPoints = 20;

    private final List<Point> m_path;

    private final double m_lookAheadDistance;
    private final double m_minVelocity;
    private final double m_maxVelocity;
    private final double m_maxSpeedUntil;
    private final double m_speedScale;
    private int m_lastClosestPointIndex = 0;

    /**
     * Creates a new Pure Pursuit controller
     * @param path the path to follow
     * @param lookAheadDistance the maximum distance the robot looks ahead to find the next point
     */
    public PurePursuitController(List<Point> path, double lookAheadDistance, double minVelocity, double maxVelocity, double maxSpeedUntil, double speedScale) {
        m_path = path;
        if (m_path.isEmpty()) {
            System.err.println("Path contains 0 points.");
        }
        m_lookAheadDistance = lookAheadDistance;
        m_minVelocity = minVelocity;
        m_maxVelocity = maxVelocity;
        m_maxSpeedUntil = maxSpeedUntil;
        m_speedScale = speedScale;
    }

    /**
     * Gets the field centric velocity to the next point
     *
     * @param  currentPose the robot's pose
     * @return the velocity the robot drives to get to the next point
     */
    public Pose2d update(Pose2d currentPose) {
        double distanceFromRobot = Double.POSITIVE_INFINITY;
        int i = m_lastClosestPointIndex;
        while (i < m_path.size()) {
            double testDistanceFromRobot = m_path.get(i).position.rangeTo(currentPose.getPosition());
            if (testDistanceFromRobot > distanceFromRobot) {
                break;
            } else {
                distanceFromRobot = testDistanceFromRobot;
            }
            i++;
        }
        Point closestPointToRobot = m_path.get(i-1);
        m_lastClosestPointIndex = i-1;

        double maxRange = 0.0;
        while (i < m_path.size() && i-m_lastClosestPointIndex < k_maxLookAheadPoints) {
            double range = m_path.get(i).position.rangeTo(currentPose.getPosition());
            if (range > m_lookAheadDistance || range < maxRange) {
                break;
            }
            maxRange = range;
            i++;
        }
        i--;
        Point lookAheadPoint = m_path.get(i);

        //field-centric
        Vector2d lookAheadPos = lookAheadPoint.position;
        Vector2d currentPos = currentPose.getPosition();
        SmartDashboard.putNumber("LookAhead X", lookAheadPos.getX());
        SmartDashboard.putNumber("LookAhead Y", lookAheadPos.getY());
        Angle angle = lookAheadPos.subtract(currentPos).getAngle();
        double velocity = m_maxVelocity;
        if ((double)i / m_path.size() > m_maxSpeedUntil)
        {
            velocity = Math.min(Math.max(m_minVelocity, closestPointToRobot.velocity * m_speedScale), m_maxVelocity);
        }
        SmartDashboard.putNumber("Velocity Angle (Deg)", angle.getValue(AngleUnit.DEGREES));
        SmartDashboard.putNumber("Velocity Magnitude", velocity);
        SmartDashboard.putNumber("Controller Heading (deg)", closestPointToRobot.heading_deg);

        return new Pose2d(new Vector2d(velocity, angle), new Angle(closestPointToRobot.heading_deg, AngleUnit.DEGREES));
    }

    public boolean isFinished() {
        return m_lastClosestPointIndex == m_path.size() - 1;
    }
}
