# Internal Library
------------------

Only should be used temporarily to develop code that could be potential library candidates. 
Any code in this folder should be moved to the
[FRC Java Library repo](https://gitlab.com/growingstems/frc-836-the-robobees/Library-Java) 
once it has been developed enough to be used throughout the robot project and other robot
projects.