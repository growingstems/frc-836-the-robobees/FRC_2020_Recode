/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import edu.wpi.first.wpilibj.DriverStation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.growingstems.math.Vector2d;
import frc.robot.FieldDimensions;

/**
 * Parses a CSV file into a list to be used with pathing.
 */
public class PathParser {

    private static final double k_maxVelocity_fps = 16.0;

    /**
     * Contains the values for the robot at a given point in a path
     */
    public static class Point {
        /** The X and Y position of the point. */
        public Vector2d position;
        /** Total distance traveled. */
        public double distance;
        /** The velocity of the robot. */
        public double velocity;
        /** The acceleration of the robot. */
        public double acceleration;
        /** The heading the robot should be facing. */
        public double heading_deg;
        /** Time elapsed since start of pathing run. */
        public double time;
    }

    private PathParser() {
    }

    /**
     * Takes in a comma seperated value file and turns it into a list to be used as
     * a path.
     *
     * @param file File to parse.
     * @return a list of type Point, or null if there was an error.
     */
    public static List<Point> parsePath(File file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            ArrayList<Point> list = new ArrayList<Point>();
            for (CSVRecord record : CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(br)) {
                Point rec = new Point();
                double x = Double.valueOf(record.get("X"));
                double y = Double.valueOf(record.get("Y"));
                rec.position = new Vector2d(FieldDimensions.k_fieldLength_in - x * 12.0, y * 12.0);
                rec.distance = Double.valueOf(record.get("Position")) * 12.0;
                rec.velocity = Double.valueOf(record.get("Velocity")) / k_maxVelocity_fps;
                rec.acceleration = Double.valueOf(record.get("Acceleration")) * 12.0;
                rec.heading_deg = 180.0 - Double.valueOf(record.get("Heading"));
                rec.time = Double.valueOf(record.get("Time"));
                list.add(rec);
            }
            return list;
        } catch (IOException e) {
            DriverStation.reportError("file " + file.getPath() + " is unreadable.\n" + e.getMessage(), true);
            return null;
        } catch (NumberFormatException e) {
            DriverStation.reportError("file " + file.getPath() + " contains values other than numbers.\n" + e.getMessage(), true);
            return null;
        } catch (IllegalStateException e) {
            DriverStation.reportError("file " + file.getPath() + " lacks header row.\n" + e.getMessage(), true);
            return null;
        } catch (IllegalArgumentException e) {
            DriverStation.reportError("file " + file.getPath() + " has incorrect column names.\n" + e.getMessage(), true);
            return null;
        }
    }

}
