/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import java.util.ArrayList;

import org.growingstems.math.Angle;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Swerve Drive is used to implement a swerve drive from any number of
 * {@link SwerveModuleBase}.
 */
public class SwerveDrive <T extends SwerveModuleBase> {
    private final ArrayList<T> m_modules;

    /**
     * Creates a SwerveDrive with the given modules.
     *
     * @param modules A list of the modules that make up the swerve drive.
     */
    public SwerveDrive(ArrayList<T> modules) {
        m_modules = modules;
    }

    /**
     * Commands all the Swerve Modules to stop.
     */
    public void stop() {
        for (T module : m_modules) {
            module.stop();
        }
    }

    /**
     * Sets the Swerve Drive into open loop control mode. This is the most basic
     * method for controlling the robot. All inputs are normalized from [-1.0, 1.0].
     *
     * @param translation The direction that the robot will be commanded to
     *                    translate towards. +X is forward for the robot and +Y is
     *                    left. This should be a normalized vector.
     * @param rotation    The amount of rotation power to be applied to the swerve
     *                    drive. [-1.0, 1.0]
     */
    public void setOpenLoop(Vector2d translation, double rotation) {
        SmartDashboard.putString("T0_0", VectorHelper.toPrintableString(translation));
        ArrayList<Vector2d> moduleVectors = inverseKinematics(translation, rotation);

        SmartDashboard.putString("T0_1", VectorHelper.toPrintableString(translation));
        SmartDashboard.putString("M0_2", VectorHelper.toPrintableString(moduleVectors.get(0)));
        setModulesOpenLoop(moduleVectors);
    }

    /**
     * Given a translation vector, and a rotation power, this method calculates the
     * vectors to be used by the swerve modules given at construction of
     * SwerveDrive. The rotation component will be applied relative to the origin of
     * the drive train. These vectors are normalized.
     *
     * @param translation The translation component used for translating the
     *                    drivetrain. This should be a normalized vector.
     * @param rotation    The rotation component used for rotating the drivetrain.
     *                    [-1.0, 1.0]
     * @return List of vectors to be used by the swerve modules. This list is
     *         ordered the same as m_modules's list.
     */
    private ArrayList<Vector2d> inverseKinematics(Vector2d translation, double rotation) {
        return inverseKinematics(translation, rotation, new Vector2d(0.0, 0.0));
    }

    /**
     * Given a translation vector, and a rotation power, this method calculates the
     * vectors to be used by the swerve modules given at construction of
     * SwerveDrive. These vectors are normalized.
     *
     * @param translation      The translation component used for translating the
     *                         drivetrain. Magnitude [0.0, 1.0]
     * @param rotation         The rotation component used for rotating the
     *                         drivetrain. [-1.0, 1.0]
     * @param centerOfRotation The point that the drivetrain will rotate about
     *                         relative to the origin of the drivetrain.
     * @return List of vectors to be used by the swerve modules. This list is
     *         ordered the same as m_modules's list.
     */
    private ArrayList<Vector2d> inverseKinematics(Vector2d translation, double rotation,
            Vector2d centerOfRotation) {
        ArrayList<Vector2d> rotationVectors = new ArrayList<>();

        for (T module : m_modules) {
            Vector2d modulePos = module.getModuleLocation();

            Vector2d rotationVector = centerOfRotation.subtract(modulePos).normalizeSet(false);
            if (Double.isNaN(rotationVector.getX()) || Double.isNaN(rotationVector.getY())) {
                rotationVectors.add(new Vector2d());
            } else {
                rotationVector.rotateSet(new Angle(-Math.PI / 2.0, AngleUnit.RADIANS));
                rotationVector.scaleSet(rotation);
                rotationVectors.add(rotationVector);
            }
        }

        SmartDashboard.putString("M0_0", VectorHelper.toPrintableString(rotationVectors.get(0)));

        if (translation.magnitude() >= 0.01) {
            for (int i = 0; i < rotationVectors.size(); i++) {
                rotationVectors.set(i, rotationVectors.get(i).add(translation));
            }
        }

        SmartDashboard.putString("M0_1", VectorHelper.toPrintableString(rotationVectors.get(0)));

        return Vector2d.normalizeGroupSet(rotationVectors, true);
    }

    /**
     * Gets the difference in position since the last time this function was called.
     * This delta in position is robot centric where +X is forwards relative to the
     * module and +Y is left relative to the module. The first time this function is
     * called, it always returns (0.0, 0.0).
     *
     * @return the difference in position since the last time this function was called.
     */
    public Vector2d getForwardKinematics() {
        Vector2d averageKinematicVector = new Vector2d();
        for(SwerveModuleBase module : m_modules){
            averageKinematicVector.addSet(module.getForwardKinematics());
        }

        return averageKinematicVector.scale(1.0 / (double)m_modules.size());
    }

    /**
     * Gets the drive's current velocity as a vector relative to the robot.
     *
     * @return Curent velocity of the drive
     */
    public Vector2d getVelocityVector() {
        Vector2d averageVelocityVector = new Vector2d();
        int i = 0;
        for(SwerveModuleBase module : m_modules){
            var moduleVelocity = module.getVelocityVector();
            averageVelocityVector.addSet(moduleVelocity);
            /*
            SmartDashboard.putNumber("Module " + i + " Vx", moduleVelocity.getX());
            SmartDashboard.putNumber("Module " + i + " Vy", moduleVelocity.getY());
            SmartDashboard.putNumber("Module " + i + " Angle", module.getSteerAngle().getValue(AngleUnit.DEGREES));
            SmartDashboard.putNumber("Module " + i + " Velocity", module.getDriveVelocity_inps());
            SmartDashboard.putNumber("Module " + i + " Raw Angle", ((SwerveModuleFalconDrvFalconStr)module).getSteerEncoder().getAbsolutePosition());
            */
            i++;
        }
        return averageVelocityVector.scale(1.0 / (double)m_modules.size());
    }


    private void setModulesOpenLoop(ArrayList<Vector2d> moduleVectors) {
        if (moduleVectors.size() != m_modules.size()) {
            return;
        }

        int i = 0;
        for (Vector2d moduleVector : moduleVectors) {
            double power = moduleVector.magnitude();
            Angle angle = Angle.atan2(moduleVector.getY(), moduleVector.getX());
            m_modules.get(i).setOpenLoop(power, angle);
            i++;
        }
    }
}
