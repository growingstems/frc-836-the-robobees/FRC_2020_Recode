/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;

/**
 * The Constants class is used to store constants that are used robot-wide. If
 * more than one component of the robot needs the same constant, and it doesn't
 * belong in RobotMap or FieldDimensions, then it probably should go here.
 */
public final class Constants {
    /**The value above which the schmitt trigger goes up. Paired with k_lowerGamepadThreshold. */
    public static final double k_upperGamepadThreshold = 0.8;
    /**The value below which schmitt trigger goes down. Paired with k_upperGamepadThreshold. */
    public static final double k_lowerGamepadThreshold = 0.4;

    /** Horizontal FOV of the limelight */
    public static final Angle k_limelightHorizontalFov = new Angle(54.0, AngleUnit.DEGREES);
    /** Vertical FOV of the limelight */
    public static final Angle k_limelightVericalFov = new Angle(41.0, AngleUnit.DEGREES);
    /** angle between the ground and the limelight lens pointing direction*/
    public static final Angle k_limelightPitch = new Angle(24.5, AngleUnit.DEGREES);
    /** height from the floor to the limelight lens */
    public static final double k_limelightHeight_in = 20.46614;

    /** distance from the center of the turret to the lens of the limelight */
    public static final double k_turretCenterToLimelightLens_in = 7.6293;
    /** distance from the center of the turret to the center of the robot */
    public static final double k_turretCenterToRobotOrigin_in = -7.748;
}
