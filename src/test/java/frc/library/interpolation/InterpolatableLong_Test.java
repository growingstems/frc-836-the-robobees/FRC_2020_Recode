/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package frc.library.interpolation;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class InterpolatableLong_Test {

    @Test
    @Parameters({
        "0,  0,  0.0, 0, Default test",
        "0,  10, 0.0, 0, 0 to 10 - 0 test",
        "0,  10, 0.1, 1, 0 to 10 - 0.1 test",
        "0,  10, 0.2, 2, 0 to 10 - 0.2 test",
        "0,  10, 0.3, 3, 0 to 10 - 0.3 test",
        "0,  10, 0.4, 4, 0 to 10 - 0.4 test",
        "0,  10, 0.5, 5, 0 to 10 - 0.5 test",
        "0,  10, 0.6, 6, 0 to 10 - 0.6 test",
        "0,  10, 0.7, 7, 0 to 10 - 0.7 test",
        "0,  10, 0.8, 8, 0 to 10 - 0.8 test",
        "0,  10, 0.9, 9, 0 to 10 - 0.9 test",
        "0,  10, 1.0, 10, 0 to 10 - 1 test",
        "0,  10, -0.5, -5, Negative extrapolate test",
        "0,  10, 2.0, 20, Positive extrapolate test",
        "10, 0, 0.0, 10, Reverse points - beginning",
        "10, 0, 0.5, 5, Reverse points - middle",
        "10, 0, 1.0, 0, Reverse points - end"
    })
    public void testInterpolate(long a, long b, double x, long expected, String testName) {
        InterpolatableLong aObj = new InterpolatableLong(a);
        InterpolatableLong bObj = new InterpolatableLong(b);

        InterpolatableLong interpolatedValue = aObj.linearInterpolate(bObj, x);
        assertEquals(testName, expected, interpolatedValue.getValue(), 1E-9);
    }

    @Test
    @Parameters({
        "0, 10, 0, 0.0, 0 to 10 - 0 test",
        "0, 10, 1, 0.1, 0 to 10 - 1 test",
        "0, 10, 2, 0.2, 0 to 10 - 2 test",
        "0, 10, 3, 0.3, 0 to 10 - 3 test",
        "0, 10, 4, 0.4, 0 to 10 - 4 test",
        "0, 10, 5, 0.5, 0 to 10 - 5 test",
        "0, 10, 6, 0.6, 0 to 10 - 6 test",
        "0, 10, 7, 0.7, 0 to 10 - 7 test",
        "0, 10, 8, 0.8, 0 to 10 - 8 test",
        "0, 10, 9, 0.9, 0 to 10 - 9 test",
        "0, 10, 10, 1.0, 0 to 10 - 10 test",
        "0, 10, -5, -0.5, Negative extrapolate test",
        "0, 10, 20, 2.0, Positive extrapolate test",
        "10, 0, 10, 0.0, Reverse points - beginning",
        "10, 0, 5, 0.5, Reverse points - middle",
        "10, 0, 0, 1.0, Reverse points - end"
    })
    public void testInverseInterpolate(long a, long b, long other, double expected, String testName) {
        InterpolatableLong aObj = new InterpolatableLong(a);
        InterpolatableLong bObj = new InterpolatableLong(b);
        InterpolatableLong otherObj = new InterpolatableLong(other);

        double x = aObj.linearInverseInterpolate(bObj, otherObj);
        assertEquals(testName, expected, x, 1E-9);
    }

    @Test
    @Parameters({
        "0, 0, 0, Equal test",
        "1, 0, 1, Greater than test",
        "0, 1, -1, Less than test"
    })
    public void testComparable(long a, long b, int expectedResult, String testName) {
        InterpolatableLong aObj = new InterpolatableLong(a);
        InterpolatableLong bObj = new InterpolatableLong(b);

        assertEquals(testName, expectedResult, aObj.compareTo(bObj));
    }
}
