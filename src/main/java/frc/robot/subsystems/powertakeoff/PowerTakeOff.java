/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.powertakeoff;

import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonSRXControlMode;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.RobotMap;
import frc.robot.subsystems.turret.Turret;

import java.util.function.Supplier;

import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;

public class PowerTakeOff extends SubsystemBase {

    private static final int k_turretMotionMagicSlot = 0;

    private final TalonSRX m_motor;
    private final Solenoid m_solenoid;
    private Angle m_postitionGoal = new Angle();

    public enum PtoState {
        HACKER_ARM_MOD, TURRET_MODE
    }

    /**
     * Creates a new PowerTakeOff subsystem.
     */
    public PowerTakeOff() {
        m_motor = new TalonSRX(RobotMap.k_ptoTalonSrxCanId);
        m_solenoid = new Solenoid(RobotMap.k_pcmCanId, RobotMap.k_ptoSolenoid);

        // Basic Config Stuff
        m_motor.configFactoryDefault(100);
        m_motor.setNeutralMode(NeutralMode.Brake);
        m_motor.setInverted(false);
        m_motor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 100);
        m_motor.setSensorPhase(true);

        // Voltage Compensation
        m_motor.configVoltageCompSaturation(12, 100);
        m_motor.enableVoltageCompensation(true);

        // Sets up soft limits
        m_motor.configForwardSoftLimitEnable(true, 100);
        m_motor.configForwardSoftLimitThreshold(Turret.k_aimSoftLimitLeft_rad * Turret.k_sensorUnitPerRadian, 100);
        m_motor.configReverseSoftLimitEnable(true, 100);
        m_motor.configReverseSoftLimitThreshold(Turret.k_aimSoftLimitRight_rad * Turret.k_sensorUnitPerRadian, 100);

        // Sets pidf
        m_motor.config_kP(k_turretMotionMagicSlot, Turret.k_motionMagicP, 100);
        m_motor.config_kI(k_turretMotionMagicSlot, Turret.k_motionMagicI, 100);
        m_motor.config_kD(k_turretMotionMagicSlot, Turret.k_motionMagicD, 100);
        m_motor.config_kF(k_turretMotionMagicSlot, Turret.k_motionMagicF, 100);
        m_motor.config_IntegralZone(k_turretMotionMagicSlot, Turret.k_turretMotionMagicIZone, 100);

        // Sets motionMagic
        m_motor.configMotionAcceleration(Turret.k_turretMotionMagicAcceleration, 100);
        m_motor.configMotionCruiseVelocity(Turret.k_turretMotionMagicVelocity, 100);

        // Offset relative encoder with absolute position on startup.
        // Relies on the the person initializing the robot to have the turret pointed relatively straight.
        // Mag Encoder outputs two signals, Quad is relative raw, and Pulse Width is absolute raw.
        int encoderAbsRawPos_su = m_motor.getSensorCollection().getPulseWidthPosition();
        encoderAbsRawPos_su = (encoderAbsRawPos_su % 4096) - (4096/2);
        m_motor.setSelectedSensorPosition(encoderAbsRawPos_su + Turret.k_absoluteEncoderOffset_su, 0, 100);

        m_motor.set(TalonSRXControlMode.Disabled, 0.0);
    }

    protected void setSolenoid(PtoState mode) {
        if (mode == PtoState.HACKER_ARM_MOD) {
            m_solenoid.set(true);
        } else {
            m_solenoid.set(false);
        }
    }

    /**
     * Gets the current position of the Turret relative to the robot.
     * <p>
     * The sensor used for the Turret is always coupled to the Turret and is
     * un-effected by the PTO. A position of 0 is straight ahead relative to the
     * robot and a positive value is counter-clockwise.
     * </p>
     *
     * @return the current position of the Turret in sensor units.
     */
    public Angle getTurretPosition() {
        return new Angle(m_motor.getSelectedSensorPosition() / Turret.k_sensorUnitPerRadian, AngleUnit.RADIANS);
    }

    protected void setPosition(Angle pos) {
        Angle halfwayBetweenSoftStops = new Angle((Turret.k_aimSoftLimitLeft_rad + Turret.k_aimSoftLimitRight_rad)/2.0, AngleUnit.RADIANS);
        double goal_rad = pos.difference(halfwayBetweenSoftStops).addSet(halfwayBetweenSoftStops).getValue(AngleUnit.RADIANS);
        goal_rad = Math.min(Math.max(Turret.k_aimSoftLimitRight_rad, goal_rad), Turret.k_aimSoftLimitLeft_rad);
        m_motor.set(TalonSRXControlMode.MotionMagic, goal_rad * Turret.k_sensorUnitPerRadian);
        m_postitionGoal = new Angle(goal_rad, AngleUnit.RADIANS);
    }

    /**
     * Gets the position goal that was last sent to the speed controller.
     *
     * @return the last position goal sent to the speed controller.
     */
    public Angle getPositionGoal() {
        return m_postitionGoal;
    }

    protected void setPower(double power) {
        m_motor.set(TalonSRXControlMode.PercentOutput, power);
    }

    protected void setStopped() {
        m_motor.set(TalonSRXControlMode.Disabled, 0.0);
    }

    public CommandBase getSetTurretPositionCommand(Supplier<Angle> pos) {
        return new FunctionalCommand(() -> setSolenoid(PtoState.TURRET_MODE), () -> setPosition(pos.get()),
                (unused) -> setStopped(), () -> false, this);
    }
}
