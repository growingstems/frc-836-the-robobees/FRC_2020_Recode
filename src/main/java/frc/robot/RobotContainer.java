/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.library.Logger;
import frc.library.PathParser;
import frc.library.PurePursuitController;
import frc.library.Util;
import frc.robot.RobotState.GalacticSearch;
import frc.robot.subsystems.drive.Drive;
import frc.robot.subsystems.hopper.Hopper;
import frc.robot.subsystems.intake.Intake;
import frc.robot.subsystems.powertakeoff.PowerTakeOff;
import frc.robot.subsystems.shooterwheel.ShooterWheel;
import frc.robot.subsystems.turret.Turret;
import frc.robot.subsystems.turret.TurretManualModeCommand;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;
import org.growingstems.signals.SchmittTrigger;
import org.growingstems.signals.api.SupplierSignalModifier;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls). Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    private static final List<Angle> lockAngles = Arrays.asList(
        new Angle(0.0, AngleUnit.ROTATIONS),
        new Angle(0.25, AngleUnit.ROTATIONS),
        new Angle(0.5, AngleUnit.ROTATIONS),
        new Angle(0.75, AngleUnit.ROTATIONS)
    );
    private enum AutoSelect {
        GALACTIC_SEARCH,
        BARREL_RACE,
        BOUNCE,
        SLALOM
    }

    private static final AutoSelect k_pathSetting = AutoSelect.SLALOM;

    // TODO: Determine units
    private static final double k_lookaheadDistance = 8.0;
    private static final double k_maxDriveSpeed = 1.0;
    private static final double k_minDeadzone = 0.1;

    // Proportional to the drive speed in ft/s
    private static final double k_driveStableGain = 0.002;
    private static final double k_lockAngle_deg = 10.0;

    private static final double k_maxDegreesPerCycle = 1.0;

    private final RobotState m_robotState;

    // The robot's subsystems and commands are defined here...
    private final Hopper m_hopper;
    private final Intake m_intake;
    private final Drive m_drive;
    private final PowerTakeOff m_pto;
    private final Turret m_turret;
    private final ShooterWheel m_shooterWheel;
    private final XboxController m_driver = new XboxController(RobotMap.k_driverUsbPort);
    private final XboxController m_operator = new XboxController(RobotMap.k_operatorUsbPort);
    private final Compressor m_compressor = new Compressor(RobotMap.k_pcmCanId);
    private final PowerDistributionPanel m_pdp;

    //TEST LOGGER
    private Logger m_log;
    private Thread m_logThread;

    /**
     * The container for the robot. Contains subsystems, OI devices, and commands.
     */
    public RobotContainer() {
        SmartDashboard.setDefaultBoolean("Lock Angles", false);
        SmartDashboard.setDefaultNumber("Drive Deadzone", k_minDeadzone);
        //create the log file
        try {
            m_log = new Logger(new File(Filesystem.getDeployDirectory(), "../Log.csv"), Arrays.asList(
                "skipped_offsets",
                "robot_pose_x",
                "robot_pose_y",
                "robot_pose_rotation",
                "shot_vector_x",
                "shot_vector_y",
                "comp_shot_vector_x",
                "comp_shot_vector_y",
                "goal_pref",
                "offset_average_x",
                "offset_average_y",
                "ready_to_shoot",
                "robot_heading",
                "velocity_vector_x",
                "velocity_vector_y",
                "shooter_shot_ready",
                "shooter_rpm",
                "shooter_state",
                "turret_angle",
                "turret_angle_goal",
                "turret_state",
                "turret_shot_ready",
                "hopper_in_shoot_state",
                "limelight_target_valid",
                "limelight_vector_x",
                "limelight_vector_y",
                "imu_fused_heading",
                "auto_select",
                "path_driven",
                "ll_raw_x",
                "ll_raw_y",
                "ll_raw_a"
            ));
            //m_logThread = new Thread(m_log);
            //m_logThread.start();
        }
        catch (IOException e) {
            System.err.println(e.getMessage());
            DriverStation.reportError("\n\n\n" + e.getMessage() + "\n\n\n", true);
            m_log = null;
        }
        m_robotState = new RobotState(m_log);

        m_pdp = new PowerDistributionPanel(RobotMap.k_pdpCanId);
        m_hopper = new Hopper(m_robotState, ()->m_pdp.getCurrent(RobotMap.k_hopperMotorChannel));
        m_drive = new Drive(m_robotState);
        //TODO: Should intake contruction be based on robot state instead of hopper?
        m_intake = new Intake(m_hopper::isInShootState);
        m_pto = new PowerTakeOff();
        m_turret = new Turret(m_robotState, m_pto::getTurretPosition, m_pto::getPositionGoal);
        m_shooterWheel = new ShooterWheel(m_robotState);
        m_robotState.init(m_shooterWheel, m_turret, m_pto, m_intake, m_hopper, m_drive);
        m_compressor.start();

        m_shooterWheel.setDefaultCommand(m_shooterWheel.getSetAutomaticCommand());

        configureButtonBindings();

    }

    /**
     * Gets the {@link RobotState} for the robot.
     *
     * @return The robot's {@link RobotState}.
     */
    public RobotState getRobotState() {
        return m_robotState;
    }
    public Logger getLogger() {
        return m_log;
    }

    /**
     * Use this method to define your button->command mappings. Buttons can bes
     * created by instantiating a {@link GenericHID} or one of its subclasses
     * ({@link edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then
     * passing it to a {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {
        SmartDashboard.setDefaultNumber("Max Drive Power", k_maxDriveSpeed);
        SmartDashboard.setDefaultNumber("Drive Deadzone", k_minDeadzone);
        var driverRightAxisTrigger = new SupplierSignalModifier<>(new SchmittTrigger(Constants.k_lowerGamepadThreshold, Constants.k_upperGamepadThreshold), () -> m_driver.getTriggerAxis(Hand.kRight));
        var driverLeftAxisTrigger = new SupplierSignalModifier<>(new SchmittTrigger(Constants.k_lowerGamepadThreshold, Constants.k_upperGamepadThreshold), () -> m_driver.getTriggerAxis(Hand.kLeft));
        var operatorRightAxisTrigger = new SupplierSignalModifier<>(new SchmittTrigger(Constants.k_lowerGamepadThreshold, Constants.k_upperGamepadThreshold), () -> m_operator.getTriggerAxis(Hand.kRight));
        var operatorLeftAxisTrigger = new SupplierSignalModifier<>(new SchmittTrigger(Constants.k_lowerGamepadThreshold, Constants.k_upperGamepadThreshold), () -> m_operator.getTriggerAxis(Hand.kLeft));

        //Intake
        Trigger intakeTrigger = new Trigger(driverLeftAxisTrigger::update);
        intakeTrigger.whenActive(m_intake.getDeployOuttakeCommand());
        Trigger stowTrigger = new Trigger(driverRightAxisTrigger::update);
        stowTrigger.whenActive(m_intake.getStowCommand());

        //Shoot
        //Trigger shootTrigger = new Trigger(operatorRightAxisTrigger::update);
        Trigger shootTrigger = new Trigger(() -> m_driver.getAButton());
        Trigger overrideShootTrigger = new Trigger(operatorLeftAxisTrigger::update);

        shootTrigger.whenActive(m_hopper.getFireCommand());
        shootTrigger.whenInactive(m_hopper.getCeaseFireCommand());

        shootTrigger.whenActive(m_shooterWheel.getFireCommand());
        shootTrigger.whenInactive(m_shooterWheel.getCeaseFireCommand());

        // Turret
        TurretManualModeCommand turretManualCommand = new TurretManualModeCommand(m_turret, m_pto, () -> {
            Vector2d shotVector = m_robotState.getCompensatedShotVector();
            Angle shotAngle = shotVector.getAngle();
            Angle heading = m_robotState.getRobotHeading();
            Angle difference = shotAngle.subtract(heading);

            return difference;
        });
        m_turret.setDefaultCommand(turretManualCommand);

        //Drive
        Supplier<Vector2d> velocitySup = () -> {
            Vector2d commandedVelocity = new Vector2d();
            double maxDriveSpeed = SmartDashboard.getNumber("Max Drive Power", k_maxDriveSpeed);
            double driveDeadzone = SmartDashboard.getNumber("Drive Deadzone", k_minDeadzone);

            //get axis with deadzone
            var driveY = Util.applyDeadzone(m_driver.getY(Hand.kLeft), driveDeadzone);
            var driveX = Util.applyDeadzone(m_driver.getX(Hand.kLeft), driveDeadzone);

            commandedVelocity.setX(driveY * -maxDriveSpeed);
            commandedVelocity.setY(driveX * -maxDriveSpeed);
            double commandedMagnitude = Math.min(commandedVelocity.magnitude(), 1.0);
            if (SmartDashboard.getBoolean("Lock Angles", false)) {
                if (commandedMagnitude != 0.0) {
                    var commandedAngle = commandedVelocity.getAngle();
                    for (var lockAngle : lockAngles) {
                        if (commandedAngle.difference(lockAngle).abs().getValue(AngleUnit.DEGREES) < k_lockAngle_deg) {
                            commandedVelocity.setPolarCoordinates(commandedMagnitude, lockAngle);
                            break;
                        }
                    }
                }
            }
            var robotVelocity_inps = m_robotState.getRobotVelocityVector();
            var perpendicularVelocity_inps = commandedVelocity.scale(robotVelocity_inps.dot(commandedVelocity) / commandedVelocity.magnitude()).subtractSet(robotVelocity_inps);
            commandedVelocity.addSet(perpendicularVelocity_inps.scaleSet(k_driveStableGain * commandedMagnitude));
            return commandedVelocity;
        };
        Angle turnAngle = new Angle(180.0, AngleUnit.DEGREES);
        m_drive.setDefaultCommand(m_drive.getTeleopNoTurningCommand(velocitySup, () -> {
            var turnPower = -m_driver.getX(Hand.kRight);
            double driveDeadzone = SmartDashboard.getNumber("Drive Deadzone", k_minDeadzone);
            if (Math.abs(turnPower) < driveDeadzone) {
                return turnAngle;
            } else {
                return turnAngle.addSet(new Angle(turnPower * k_maxDegreesPerCycle, AngleUnit.DEGREES));
            }
        }));
        //m_drive.setDefaultCommand(m_drive.getDirectDriveCommand(velocitySup,
                                                                //() -> m_driver.getX(Hand.kRight)));

        Trigger unlockFullPowerTriggerA = new Trigger(() -> m_driver.getBackButton());
        Trigger unlockFullPowerTriggerB = new Trigger(() -> m_driver.getStickButton(Hand.kRight));
        Trigger unlockFullPowerTriggerC = new Trigger(() -> m_driver.getBButton());
        Trigger unlockFullPowerTriggerD = new Trigger(() -> m_driver.getYButton());
        Trigger unlockFullPowerTrigger = unlockFullPowerTriggerA.and(unlockFullPowerTriggerB).and(unlockFullPowerTriggerC)
            .and(unlockFullPowerTriggerD);
        Trigger parkTrigger = new Trigger(() -> m_driver.getBumper(Hand.kRight));
        Trigger slowModeTrigger = new Trigger(() -> m_driver.getBumper(Hand.kLeft));
        Trigger reorientTrigger = new Trigger(() -> m_driver.getStartButton());
        Trigger positionOverrideUpTrigger = new Trigger(() -> m_operator.getPOV() == 0);
        Trigger positionOverrideRightTrigger = new Trigger(() -> m_operator.getPOV() == 90);
        Trigger positionOverrideDownTrigger = new Trigger(() -> m_operator.getPOV() == 180);
        Trigger positionOverrideLeftTrigger = new Trigger(() -> m_operator.getPOV() == 270);

        //Hacker Arm
        Trigger stage2Trigger = new Trigger(() -> m_operator.getAButton());
        Trigger stage3Trigger = new Trigger(() -> m_operator.getBButton());
        Trigger deployHackerArmTrigger = new Trigger(() -> m_operator.getXButton());

        //Climb
        Trigger driverConfirmClimbTrigger = new Trigger(() -> m_driver.getPOV() == 0);
        Trigger deployClimbPistonATrigger = new Trigger(() -> m_operator.getStartButton());
        Trigger deployClimbPistonBTrigger = new Trigger(() -> m_operator.getBackButton());
        Trigger deployClimbPistonTrigger = deployClimbPistonATrigger.and(deployClimbPistonBTrigger);
        Trigger climbHangATrigger = new Trigger(() -> m_operator.getBackButton());
        Trigger climbHangBTrigger = new Trigger(() -> m_operator.getBumper(Hand.kRight));
        Trigger climbHangTrigger = climbHangATrigger.and(climbHangBTrigger);
        Trigger climbLevelTrigger = new Trigger(() -> m_operator.getYButton());
        Trigger climbMoreTrigger = new Trigger(() -> m_operator.getPOV() == 0);
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        String filename = "";
        double maxSpeed = 1.0;
        double speedScale = 2.0;
        GalacticSearch pathEnum = null;
        double maxSpeedUntil = 0.0;
        switch (k_pathSetting) {
            case BARREL_RACE:
                filename = "paths/BarrelRace.csv";
                break;
            case BOUNCE:
                filename = "paths/Bounce.csv";
                break;
            case SLALOM:
                filename = "paths/Slalom.csv";
                break;
            case GALACTIC_SEARCH:
                maxSpeedUntil = 0.0;
                pathEnum = m_robotState.determineGalacticPath();
                m_log.setDataPoint("path_driven", pathEnum.ordinal());
                SmartDashboard.putString("Chosen Path", pathEnum.toString());
                switch (pathEnum) {
                    case RED_A:
                        filename = "paths/GalacticSearchPartARed.csv";
                        break;
                    case RED_B:
                        filename = "paths/GalacticSearchPartBRed.csv";
                        break;
                    case BLUE_A:
                        filename = "paths/GalacticSearchPartABlue.csv";
                        break;
                    case BLUE_B:
                        filename = "paths/GalacticSearchPartBBlue.csv";
                        break;
                    default:
                        DriverStation.reportError("Invalid path enum received: " + pathEnum.toString(), false);
						break;
                }
                break;
        }
        var path = PathParser.parsePath(new File(Filesystem.getDeployDirectory(), filename));
        var firstElem = path.get(0);
        m_robotState.setRobotPose(new Pose2d(firstElem.position, new Angle(firstElem.heading_deg, AngleUnit.DEGREES)));

        var controller = new PurePursuitController(path, k_lookaheadDistance, 0.5, maxSpeed, maxSpeedUntil, speedScale);

        //log auto settings
        m_log.setDataPoint("auto_select", k_pathSetting.ordinal());
        Command driveCommand = m_drive.getVelocityAndHeadingCommand(
            () -> controller.update(m_robotState.getUnadjustedRobotPose()),
            controller::isFinished);

        if (pathEnum != null) {
            return new SequentialCommandGroup(
                new ParallelCommandGroup(driveCommand,
                                         m_intake.getDeployIntakeCommand()),
                m_intake.getStowCommand());
        } else if (k_pathSetting == AutoSelect.BOUNCE) {
            return new SequentialCommandGroup(
                new ParallelCommandGroup(driveCommand,
                                         m_intake.getDeployIdleCommand()),
                m_intake.getStowCommand());
        } else {
            return driveCommand;
        }
    }

    public void onDisable() {
        m_robotState.onDisable();
    }
}
