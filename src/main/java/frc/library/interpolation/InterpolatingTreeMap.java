/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.interpolation;

import java.util.Map;
import java.util.TreeMap;

/**
 * InterpolatingTreeMap is a class used to store an organized list of data
 * points that can be interpolated when the given key isn't found. The data
 * points are stored as a Red-Black tree which allows for both quick acquisition
 * and the ability to find neighboring keys if an exact match isn't found.
 * <p>
 * The class used for the Keys (K) must implement InverseInterpolatable, and
 * should implement Comparable. For more information, refer to TreeMap's
 * documentation.
 * <p>
 * The class used for the Values (V) must implement Interpolatable.
 *
 * @param <K> The type of the key (must implement InverseInterpolatable, should implement Comparable).
 * @param <V> The type of the value (must implement Interpolatable).
 */
public class InterpolatingTreeMap<K extends InverseInterpolatable<K>, V extends Interpolatable<V>>
        extends TreeMap<K, V> {

    private static final long serialVersionUID = 5564010746135758206L;

    /**
     * Gets a value based on the key given. If the key isn't found then an
     * interpolated value is returned instead.
     * <p>
     * If the given key is outside the bounds of the current TreeMap:
     * <ul>
     * <li>extrapolate == false - The closest key's value is returned if possible.
     * <li>extrapolate == true - An extrapolated value is returned if possible.
     * </ul>
     *
     * @param key The key used to find/extrapolate a value.
     * @param extrapolate Set true if extrapolating a value is desired.
     * @return The found/interpolated/extrapolated value.
     */
    public V getValue(K key, boolean extrapolate) {
        // There needs to be values in order to get one.
        if (size() == 0) {
            return null;
        }

        V value = get(key);
        // Return the value if the key is exact
        if (value != null) {
            return value;
        }

        Map.Entry<K, V> ceiling = ceilingEntry(key);
        Map.Entry<K, V> floor = floorEntry(key);

        // Should've been caught by the 0 size check. Couldn't find any keys based on
        // the given key.
        if (ceiling == null && floor == null) {
            return null;
        }

        // We are between two keys. Interpolate value
        if (floor != null && ceiling != null) {
            double x = floor.getKey().linearInverseInterpolate(ceiling.getKey(), key);
            return floor.getValue().linearInterpolate(ceiling.getValue(), x);
        }

        // Outside of map bounds. Extrapolate if enabled and if the tree is big enough
        if (extrapolate && size() > 1) {
            if (ceiling == null) {
                Map.Entry<K, V> lowerEntry = lowerEntry(floor.getKey());

                if (lowerEntry != null) {
                    double x = lowerEntry.getKey().linearInverseInterpolate(floor.getKey(), key);
                    return lowerEntry.getValue().linearExtrapolate(floor.getValue(), x);
                }
            } else if (floor == null) {
                Map.Entry<K, V> higherEntry = higherEntry(ceiling.getKey());

                if (higherEntry != null) {
                    double x = higherEntry.getKey().linearInverseInterpolate(ceiling.getKey(), key);
                    return higherEntry.getValue().linearExtrapolate(ceiling.getValue(), x);
                }
            }
        }

        // Outside of map bounds and not set to extrapolate. Return the closest value.
        if (floor == null) {
            return ceiling.getValue();
        } else if (ceiling == null) {
            return floor.getValue();
        } else {
            return null;
        }
    }
}
