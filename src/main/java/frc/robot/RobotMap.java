/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class RobotMap {
    //===================================================================
    // USB Devices
    //===================================================================
    //controllers
    public static final int k_driverUsbPort = 0;
    public static final int k_operatorUsbPort = 1;

    //===================================================================
    // CAN Devices
    //===================================================================
    //PDP
    public static final int k_pdpCanId = 50;

    //PCM
    public static final int k_pcmCanId = 15;

    //Swerve Drive
    public static final int k_swerveFrontRightPowerCanId = 1;
    public static final int k_swerveFrontRightSteerCanId = 2;
    public static final int k_swerveFrontLeftPowerCanId = 3;
    public static final int k_swerveFrontLeftSteerCanId = 4;
    public static final int k_swerveBackLeftPowerCanId = 5;
    public static final int k_swerveBackLeftSteerCanId = 6;
    public static final int k_swerveBackRightPowerCanId = 7;
    public static final int k_swerveBackRightSteerCanId = 8;
    public static final int k_swerveFrontRightCanCoderCanId = 9;
    public static final int k_swerveFrontLeftCanCoderCanId = 10;
    public static final int k_swerveBackLeftCanCoderCanId = 11;
    public static final int k_swerveBackRightCanCoderCanId = 12;

    //Intake
    public static final int k_intakeMotorCanId = 30;

    //Hopper
    public static final int k_hopperMotorCanId = 31;

    //PTO
    public static final int k_ptoTalonSrxCanId = 23;

    //IMU
    public static final int k_pigeonImuCanId = 16;

    //===================================================================
    // Pneumatics
    //===================================================================
    //Intake
    public static final int k_intakeSolenoid = 0;

    //PTO
    public static final int k_ptoSolenoid = 3;

    //===================================================================
    // DIO
    //===================================================================
    //Hopper
    public static final int k_dioHopperPrime = 2;
    public static final int k_dioHopperInner = 0;
    public static final int k_dioHopperOuter = 1;

    //===================================================================
    // Analog
    //===================================================================

    //===================================================================
    // PWM
    //===================================================================

    //===================================================================
    // Relay
    //===================================================================

    //===================================================================
    //PDB Channels
    //===================================================================
    //Hopper
    public static final int k_hopperMotorChannel = 7;
}
