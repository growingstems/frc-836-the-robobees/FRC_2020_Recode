/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;

public class VectorHelper {
    public static String toPrintableString(Vector2d vector) {
        return Math.round(100 * vector.magnitude()) + "@" + Math.round(vector.getAngle().getValue(AngleUnit.DEGREES));
    }
}
