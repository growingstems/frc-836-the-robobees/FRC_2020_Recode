/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems.turret;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.subsystems.powertakeoff.PowerTakeOff;

import java.util.function.Supplier;

import org.growingstems.math.Angle;

public class TurretManualModeCommand extends CommandBase {

    private final CommandBase m_ptoSetTurretPositionCommand;
    private final Command m_turretSetTurretModeCommand;

    public TurretManualModeCommand(final Turret turret, final PowerTakeOff pto, Supplier<Angle> turrentPos) {
        addRequirements(turret, pto);
        m_ptoSetTurretPositionCommand = pto.getSetTurretPositionCommand(turrentPos);
        m_turretSetTurretModeCommand = turret.getManualCommand();
    }

    @Override
    public void initialize() {
        m_ptoSetTurretPositionCommand.initialize();
        m_turretSetTurretModeCommand.initialize();
    }

    @Override
    public void execute() {
        m_ptoSetTurretPositionCommand.execute();
        m_turretSetTurretModeCommand.execute();
    }

    @Override
    public void end(final boolean interrupted) {
        m_ptoSetTurretPositionCommand.end(interrupted);
        m_turretSetTurretModeCommand.end(interrupted);
    }

    @Override
    public boolean isFinished() {
        return m_ptoSetTurretPositionCommand.isFinished() && m_turretSetTurretModeCommand.isFinished();
    }
}
