package frc.library;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.growingstems.math.Vector2d;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class SwerveDrive_Test {

    @Test
    @Parameters({ "0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Default test",
            "1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0 Forward test",
            "0.0, 1.0, 0.0, 1.0, 90.0, 1.0, 90.0, 1.0, 90.0, 1.0, 90.0, 1.0 Left test",
            "0.0, 0.0, 1.0, 1.0, 45.0, -1.0, -45.0, -1.0, 45.0, 1.0, -45.0, 1.0 Turn Left test",
            "0.0, 0.0, -1.0, -1.0, 45.0, 1.0, -45.0, 1.0, 45.0, -1.0, -45.0, 1.0 Turn Right test",
            "0.5, 0.0, 0.0, 0.5, 0.0, 0.5, 0.0, 0.5, 0.0, 0.5, 0.0, Partial Speed Test",
            "0.0, 0.0, 0.5, 0.5, 45.0, 0.5, 135.0, 0.5, -135.0, 0.5, -45.0, Parial Turn Left Test" })
    public void setOpenLoopTest(double x, double y, double rotation, double expPow1, double expAng1, double expX2,
            double expY2, double expX3, double expY3, double expX4, double expY4, String testName) {

        ArrayList<SwerveModuleBase> swerveModules = new ArrayList<>();
        SwerveModuleBase_Concrete module1 = new SwerveModuleBase_Concrete(new Vector2d(1.0, -1.0), 1.0, 1.0); // Front Right
        swerveModules.add(module1);
        SwerveModuleBase_Concrete module2 = new SwerveModuleBase_Concrete(new Vector2d(1.0, 1.0), 1.0, 1.0); // Front Left
        swerveModules.add(module2);
        SwerveModuleBase_Concrete module3 = new SwerveModuleBase_Concrete(new Vector2d(-1.0, 1.0), 1.0, 1.0); // Back Left
        swerveModules.add(module3);
        SwerveModuleBase_Concrete module4 = new SwerveModuleBase_Concrete(new Vector2d(-1.0, -1.0), 1.0, 1.0); // Back Right
        swerveModules.add(module4); 

        SwerveDrive<?> swerveDrive = new SwerveDrive<>(swerveModules);

        swerveDrive.setOpenLoop(new Vector2d(x, y), rotation);
        assertEquals("(" + testName + ") - Module 1(Power).", expPow1, module1.getDriveOpenLoop_argPower(), 1.0E-9);
        assertEquals("(" + testName + ") - Module 1(Angle).", expAng1, Math.toDegrees(module1.getSteerPositionClosedLoop_argPosition()), 1.0E-9);
    }
}
