/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
package frc.robot.subsystems.climb.commands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;

import frc.robot.subsystems.intake.Intake;

/**
 * Make sure the robot is in a state to pickup soda cans.
 */
public class DeployClimb extends ParallelCommandGroup {
  public DeployClimb(Intake intake) {
    // addCommands(
    //     intake.getExtender().getDeployCommand(),
    //     intake.getRoller().getIntakeCommand()
    // );
  }
}
