package frc.library;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class Util_Test {

    @Test
    @Parameters({
        "0.0, 0.0, 0.0",
        "0.0, 179.9, 0.0",
        "0.0, 180.0, 360.0",
        "-0.1, 180.0, 359.9",
        "0.0, 180.1, 360.0",
        "-180.0, 180.0, 180.0",
        "-179.0, 179.0, 181.0",
        "90.0, -270.0, -270.0",
        "-90.0, -270.0, -450.0",
    })
    public void shiftWrappedAngleScopeTest(double wrapAng, double reference, double expected) {
       //var swerveModule = new SwerveModuleFalconDrvFalconStr();

       assertEquals("Test", Math.toRadians(expected), Util.shiftWrappedAngleScope(Math.toRadians(wrapAng), Math.toRadians(reference)), 1E-6);
    }

    @Test
    @Parameters({
        "0.0, 0.0, 0.0",
        "1.0, 0.0, 1.0",
        "0.1, 0.3, 0.0",
        "1.0, 0.3, 1.0",
        "0.5, 0.5, 0.0",
        "0.6, 0.5, 0.2",
        "0.7, 0.5, 0.4",
        "0.8, 0.5, 0.6",
        "0.9, 0.5, 0.8",
        "1.0, 0.5, 1.0",
        "0.359733, 0.899735, 0.0",
        "0.925935, 0.23915, 0.9026549",
        "0.772818, 0.642637, 0.3642822",
        "0.506766, 0.278894, 0.3160034",
        "0.780831, 0.587823, 0.4682648",
        "0.00478729, 0.26723, 0.0",
        "0.0487221, 0.985908, 0.0",
        "0.0398191, 0.691851, 0.0",
        "0.625701, 0.64928, 0.0",
        "0.506137, 0.830189, 0.0",
        "-1.0, 0.0, -1.0",
        "-0.1, 0.3, -0.0",
        "-1.0, 0.3, -1.0",
        "-0.5, 0.5, -0.0",
        "-0.6, 0.5, -0.2",
        "-0.7, 0.5, -0.4",
        "-0.8, 0.5, -0.6",
        "-0.9, 0.5, -0.8",
        "-1.0, 0.5, -1.0",
        "-0.359733, 0.899735, 0.0",
        "-0.925935, 0.23915, -0.9026549",
        "-0.772818, 0.642637, -0.3642822",
        "-0.506766, 0.278894, -0.3160034",
        "-0.780831, 0.587823, -0.4682648",
        "-0.00478729, 0.26723, 0.0",
        "-0.0487221, 0.985908, 0.0",
        "-0.0398191, 0.691851, 0.0",
        "-0.625701, 0.64928, 0.0",
        "-0.506137, 0.830189, 0.0",
    })
    public void applyDeadzoneTest(double joystickInput, double deadZone, double expected) {
        var output = Util.applyDeadzone(joystickInput, deadZone);

        assertEquals(expected, output, 1E-5);
    }
}
