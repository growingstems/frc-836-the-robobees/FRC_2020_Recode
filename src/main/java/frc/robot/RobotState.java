/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import java.util.Arrays;
import java.util.List;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.sensors.PigeonIMU;

import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.util.Pair;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.library.CircularBuffer;
import frc.library.Logger;
import frc.robot.FieldDimensions.GoalType;
import frc.robot.subsystems.drive.Drive;
import frc.robot.subsystems.hopper.Hopper;
import frc.robot.subsystems.intake.Intake;
import frc.robot.subsystems.limelight.Limelight;
import frc.robot.subsystems.powertakeoff.PowerTakeOff;
import frc.robot.subsystems.shooterwheel.ShooterWheel;
import frc.robot.subsystems.turret.Turret;

/**
 * This class holds all the calulations and resources needed to maintain the
 * robot's state. Generally these calculations are made at the beginning of
 * every loop so that all of the robot's subsystems can get access to the most
 * up-to-date state data. To initiate these calculations, call updateState().
 *
 * <pre>
 * The state of the robot includes:
 * </pre>
 * <ul>
 * <li>The robot's pose relative to the field.
 * <li>The turret's pose relative to the field.
 * </ul>
 */
public class RobotState {
    private static final String k_standardDeviationKey = "STD Dev";
    private static final String k_skippedOffsetKey = "# Skipped Offsets";
    private static final int k_offsetDequeSize = 5;
    private static final int k_limelightHistorySize = 5;
    private static final double k_offsetDiscardDistance_in = 50.0;

    public enum GalacticSearch {
        RED_A,
        RED_B,
        BLUE_A,
        BLUE_B
    }

    private static final List<Pair<GalacticSearch, Vector2d>> m_galacticSearchPoints = Arrays.asList(
            new Pair<>(GalacticSearch.BLUE_A, new Vector2d(0.92, -3.00)),
            new Pair<>(GalacticSearch.RED_A, new Vector2d(22.00, -9.75)),
            new Pair<>(GalacticSearch.BLUE_B, new Vector2d(-12.00, -2.97)),
            new Pair<>(GalacticSearch.RED_B, new Vector2d(27.91, -10.75))
        );

    private final Logger m_log;
    private final OffsetHistory m_offsetHistory = new OffsetHistory();
    private final CircularBuffer<Vector2d> m_limelightHistory = new CircularBuffer<>(k_limelightHistorySize);

    private int m_skippedOffsetCount = 0;
    private double m_offsetHeading_deg = 0.0;

    private Pose2d m_robotPose = new Pose2d(new Vector2d(), new Angle(180.0, Angle.AngleUnit.DEGREES));
    private Vector2d m_shotVector = new Vector2d();
    private Vector2d m_compensatedShotVector = new Vector2d();
    // TODO: Decide what the goal preference is, possibly in a seperate method.
    private FieldDimensions.GoalType m_goalPreference = GoalType.OUTER;

    // Subsytem References
    private ShooterWheel m_shooterWheel;
    private Turret m_turret;
    private PowerTakeOff m_pto;
    private Intake m_intake;
    private Hopper m_hopper;
    private Drive m_drive;
    private final Limelight m_limelight = new Limelight(Constants.k_limelightPitch, Constants.k_limelightHeight_in);
    private final PigeonIMU m_imu = new PigeonIMU(RobotMap.k_pigeonImuCanId);

    private static class OffsetHistory {
        private final CircularBuffer<Vector2d> m_offsets = new CircularBuffer<>(k_offsetDequeSize);
        private final static StandardDeviation m_stdDevCalculator = new StandardDeviation();
        private Vector2d m_average = null;
        private Double m_stdDev = null;

        private void add(Vector2d offset) {
            m_offsets.add(offset);
            m_average = null;
            m_stdDev = null;
        }

        private boolean isFull() {
            return m_offsets.size() == m_offsets.getSize();
        }

        private void clear() {
            m_offsets.clear();
        }

        private Vector2d averageOffset() {
            if (m_average != null) {
                return m_average;
            }

            m_average = new Vector2d();
            if (m_offsets.getSize() == 0) {
                return m_average;
            }

            for (Vector2d vec : m_offsets) {
                m_average.addSet(vec);
            }

            return m_average.scaleSet(1.0 / m_offsets.getSize());
        }

        private double standardDeviation() {
            if (m_stdDev != null) {
                return m_stdDev;
            }

            Vector2d stdDev = new Vector2d();
            double[] values = new double[m_offsets.size()];
            int i = 0;
            for (Vector2d offset : m_offsets) {
                values[i] = offset.getX();
                i++;
            }
            stdDev.setX(m_stdDevCalculator.evaluate(values));
            i = 0;
            for (Vector2d offset : m_offsets) {
                values[i] = offset.getY();
                i++;
            }
            stdDev.setY(m_stdDevCalculator.evaluate(values));
            m_stdDev = stdDev.magnitude();
            return m_stdDev;
        }
    }

    public RobotState(Logger log) {
        m_log = log;
        //Ensure the IMU reports 0 when the robot starts (since code reruns don't necessarily garuntee IMU reset)
        m_imu.setYaw(0.0);
        m_imu.setFusedHeading(0.0);
        SmartDashboard.setDefaultNumber(k_skippedOffsetKey, 0);
        SmartDashboard.setDefaultNumber(k_standardDeviationKey, 0);
    }

    /**
     * Initializes the RobotState class. Must be provided with references to the
     * Robot's various subsystems.
     *
     * @param shooterWheel a reference to the ShooterWheel subsystem.
     * @param turret       a reference to the Turret subsystem.
     * @param intake       a reference to the Intake subsystem.
     * @param hopper       a reference to the Hopper subsystem.
     * @param drive        a reference to the Drive subsystem.
     */
    public void init(ShooterWheel shooterWheel, Turret turret, PowerTakeOff pto, Intake intake, Hopper hopper, Drive drive) {
        m_shooterWheel = shooterWheel;
        m_turret = turret;
        m_pto = pto;
        m_intake = intake;
        m_hopper = hopper;
        m_drive = drive;
    }

    public void onDisable() {
        m_offsetHistory.clear();
    }

    /**
     * This method initiates the collection of external sensor data and calculates
     * all the relevent RobotState data. This method generally will be first thing
     * to run before any other robot code.
     */
    public void updateState() {
        SmartDashboard.putNumber("TAngle", m_turret.getAngle().getValue(AngleUnit.DEGREES));
        calculateOdometry();
        calculateOffset();
        calculateShotVector();
        calculateCompensatedShotVector();
        determineGalacticPath();

        //add log details
        m_log.setDataPoint("skipped_offsets", m_skippedOffsetCount);
        m_log.setDataPoint("robot_pose_x", m_robotPose.getX());
        m_log.setDataPoint("robot_pose_y", m_robotPose.getY());
        m_log.setDataPoint("robot_pose_rotation", m_robotPose.getRotation().getValue(AngleUnit.DEGREES));
        m_log.setDataPoint("shot_vector_x", m_shotVector.getX());
        m_log.setDataPoint("shot_vector_y", m_shotVector.getY());
        m_log.setDataPoint("comp_shot_vector_x", m_compensatedShotVector.getX());
        m_log.setDataPoint("comp_shot_vector_y", m_compensatedShotVector.getY());
        m_log.setDataPoint("goal_pref", m_goalPreference.ordinal());
        m_log.setDataPoint("offset_average_x", m_offsetHistory.averageOffset().getX());
        m_log.setDataPoint("offset_average_y", m_offsetHistory.averageOffset().getY());
        m_log.setDataPoint("ready_to_shoot", (isReadyToShoot() ? 1.0 : 0.0));
        m_log.setDataPoint("robot_heading", getRobotHeading().getValue(AngleUnit.DEGREES));
        var velVec = getRobotVelocityVector();
        m_log.setDataPoint("velocity_vector_x", velVec.getX());
        m_log.setDataPoint("velocity_vector_y", velVec.getY());

        //shooter wheel logs
        m_log.setDataPoint("shooter_shot_ready", (m_shooterWheel.isReadyToShoot() ? 1.0 : 0.0));
        m_log.setDataPoint("shooter_rpm", m_shooterWheel.getSpeed_RPM());
        m_log.setDataPoint("shooter_state", m_shooterWheel.getState().ordinal());

        //turret logs
        m_log.setDataPoint("turret_angle", m_turret.getAngle().getValue(AngleUnit.DEGREES));
        m_log.setDataPoint("turret_angle_goal", m_turret.getAngleGoal().getValue(AngleUnit.DEGREES));
        var turretState = m_turret.getState();
        m_log.setDataPoint("turret_state", turretState == null ? -1 : turretState.ordinal());
        m_log.setDataPoint("turret_shot_ready", (m_turret.isReadyToShoot() ? 1.0 : 0.0));

        //pto logs (none needed (probably))

        //intake logs

        //hopper logs
        m_log.setDataPoint("hopper_in_shoot_state", (m_hopper.isInShootState() ? 1.0 : 0.0));

        //limelight logs
        var llVector = m_limelight.getTargetVector_in();
        if (llVector == null) {
            m_log.setDataPoint("limelight_target_valid", 0.0);
            m_log.setDataPoint("limelight_vector_x", 0.0);
            m_log.setDataPoint("limelight_vector_y", 0.0);
            m_log.setDataPoint("ll_raw_x", 0.0);
            m_log.setDataPoint("ll_raw_y", 0.0);
            m_log.setDataPoint("ll_raw_a", 0.0);
        }
        else {
            m_log.setDataPoint("limelight_target_valid", 1.0);
            m_log.setDataPoint("limelight_vector_x", llVector.getX());
            m_log.setDataPoint("limelight_vector_y", llVector.getY());
            m_log.setDataPoint("ll_raw_x", m_limelight.getAngleToTargetVertical().getValue(AngleUnit.DEGREES));
            m_log.setDataPoint("ll_raw_y", m_limelight.getAngleToTargetHorizontal().getValue(AngleUnit.DEGREES));
            m_log.setDataPoint("ll_raw_a", m_limelight.getTargetArea());
        }

        //m_log.setDataPoint("imu_fused_heading", m_imu.getFusedHeading());

    //private final OffsetHistory m_offsetHistory = new OffsetHistory();
    }

    private void calculateShotVector() {
        Vector2d latestTurretPosition = getLatestTurretPose().getPosition();
        if (m_goalPreference == FieldDimensions.GoalType.INNER) {
            //m_shotVector = FieldDimensions.k_powerPortInnerCenter_in.subtract(latestTurretPosition);
        } else {
            m_shotVector = FieldDimensions.k_powerPortOuterCenter_in.subtract(latestTurretPosition);
        }
    }

    /**
     * Returns the lastest field-oriented pose of the robot.
     *
     * @return The latest field-oriented pose of the robot.
     */
    public Pose2d getLatestRobotPose() {
        var pose = m_robotPose.transform(m_offsetHistory.averageOffset(), new Angle());
        SmartDashboard.putNumber("Robot X", pose.getX());
        SmartDashboard.putNumber("Robot Y", pose.getY());
        SmartDashboard.putNumber("Robot Heading", pose.getRotation().getValue(AngleUnit.DEGREES));
        return pose;
    }

    /**
     * Returns the latest field-oriented pose of the robot, without considering Limelight
     *
     * @return The latest field-oriented pose of the robot.
     */
    public Pose2d getUnadjustedRobotPose() {
        return new Pose2d(m_robotPose);
    }

    /**
     * Returns an interpolated/extrapolated field-oriented pose of the robot based
     * on a history of collected data points.
     *
     * @param time_us The time in microseconds used to find the robot's pose.
     * @return An interpolated/extrapolated field-oriented pose of the robot.
     */
    public Pose2d getRobotPose(long time_us) {
        // TODO: Implement this method.
        return null;
    }

    /**
     * Returns the lastest field-oriented Turret Pose.
     *
     * @return The latest field-oriented pose of the turret.
     */
    public Pose2d getLatestTurretPose() {
        Pose2d robotPose = getLatestRobotPose();
        return robotPose.transform(Turret.k_turretCenterToRobotOrigin_in.rotate(robotPose.getRotation()), m_turret.getAngle());
    }

    /**
     * Returns an interpolated/extrapolated field-oriented pose of the turret based
     * on a history of collected data points.
     *
     * @param time_us The time in microseconds used to find the turret's pose.
     * @return An interpolated/extrapolated field-oriented pose of the robot.
     */
    public Pose2d getTurretPose(long time_us) {
        // TODO: Implement this method.
        return null;
    }

    /**
     * Used to check what the current shot vector that the robot is currently aiming
     * with. This is a vector that extends from the center of the turret to a point
     * below the center of the prefered goal, projected on a plane parallel to the
     * field. This vector is relative to the field.
     *
     * @return the currently calculated shot vector based on the goal preference.
     */
    public Vector2d getShotVector() {
        // TODO: Make this a 3D vector once Translation3d gets implemented.
        return m_shotVector;
    }

    /**
     * Used to check which goal the robot currently prefers to shoot at.
     *
     * @return the current goal that the robot prefers to shoot at.
     */
    public FieldDimensions.GoalType getGoalPreference() {
        return m_goalPreference;
    }

    /**
     * Used to check if the robot is ready to shoot.
     *
     * @return if the robot is currently ready to shoot.
     */
    public boolean isReadyToShoot() {
        return m_turret.isReadyToShoot() && m_shooterWheel.isReadyToShoot();
    }

    /**
     * Used to check if the robot is currently in the designated zone where a shot
     * is possible.
     *
     * @return if the robot is in the shootable zone.
     */
    public boolean isInShootZone() {
        // TODO: Implement this function.
        return true;
    }

    public void setRobotPose(Pose2d pose) {
        m_robotPose = pose;
        double angle_deg = pose.getRotation().getValue(AngleUnit.DEGREES);

        double[] init_ypr_deg = new double[3];
        m_imu.getYawPitchRoll(init_ypr_deg);
        SmartDashboard.putNumber("SRP_init_yaw", init_ypr_deg[0]);

        m_imu.setYaw(angle_deg);
        var errorCode = m_imu.setFusedHeading(angle_deg);
        if (errorCode != ErrorCode.OK) {
            DriverStation.reportError("IMU Reported error: " + errorCode.toString(), false);
        }
        //double instantReturn = m_imu.getFusedHeading();
        double[] ypr_deg = new double[3];
        m_imu.getYawPitchRoll(ypr_deg);
        //m_offsetHeading_deg = angle_deg - ypr_deg[0];
        SmartDashboard.putNumber("SET", angle_deg);
        SmartDashboard.putNumber("GET", ypr_deg[0]);
        DriverStation.reportError("SET " + angle_deg + " | GET " + ypr_deg[0], false);
        SmartDashboard.putNumber("SRP_Offset", m_offsetHeading_deg);
        m_offsetHistory.clear();
    }

    public GalacticSearch determineGalacticPath() {
        m_limelightHistory.add(new Vector2d(m_limelight.getAngleToTargetVertical().getValue(AngleUnit.DEGREES),
                                            m_limelight.getAngleToTargetHorizontal().getValue(AngleUnit.DEGREES)));
        Vector2d target = new Vector2d();
        for (var limelightVector : m_limelightHistory) {
            target.addSet(limelightVector);
        }
        target.scaleSet(1.0 / m_limelightHistory.size());
        SmartDashboard.putNumber("gs_limelight_angle_vert", target.getX());
        SmartDashboard.putNumber("gs_limelight_angle_hor", target.getY());
        SmartDashboard.putBoolean("gs_limelight_valid", m_limelight.getValidTarget());
        GalacticSearch closest = null;
        double closestDist = Double.MAX_VALUE;
        for (var point : m_galacticSearchPoints) {
            var dist = point.getY().rangeTo(target);
            if (closestDist > dist) {
                closestDist = dist;
                closest = point.getX();
            }
        }

        SmartDashboard.putString("auto path", closest.toString());
        return closest;
    }

    /**
     * Used to calculate the robot's current field-centric position
     */
    private void calculateOdometry() {
        Vector2d deltaPos = m_drive.getForwardKinematics();
        Angle headingAngle = getRobotHeading();
        m_robotPose = m_robotPose.transform(deltaPos.rotate(headingAngle),
            headingAngle.subtract(m_robotPose.getRotation()));
        SmartDashboard.putNumber("Robot Odo X", m_robotPose.getX());
        SmartDashboard.putNumber("Robot Odo Y", m_robotPose.getY());
    }

    private void calculateOffset() {
        Vector2d targetVector_in = m_limelight.getTargetVector_in();
        if(targetVector_in != null){
            targetVector_in.addSet(new Vector2d(Constants.k_turretCenterToLimelightLens_in, 0.0));
            targetVector_in.rotateSet(m_turret.getAngle());
            targetVector_in.addSet(Turret.k_turretCenterToRobotOrigin_in);
            targetVector_in.rotateSet(getRobotHeading());
            Vector2d robotPosition = FieldDimensions.k_powerPortOuterCenter_in.subtract(targetVector_in);
            Vector2d offset = robotPosition.subtract(m_robotPose.getPosition());
            double mag = offset.subtract(m_offsetHistory.averageOffset()).magnitude();
            SmartDashboard.putNumber("OffsetError", mag);
            if (mag < k_offsetDiscardDistance_in || !m_offsetHistory.isFull()) {
                m_offsetHistory.add(offset);
                SmartDashboard.putNumber(k_standardDeviationKey, m_offsetHistory.standardDeviation());
            } else {
                m_skippedOffsetCount++;
                SmartDashboard.putNumber(k_skippedOffsetKey, m_skippedOffsetCount);
            }
        }
    }

    /**
     * Used to get robot's current heading
     *
     * @return Robot heading as a bounded Angle
     */
    public Angle getRobotHeading() {
        //difference with a default angle bounds the returned value
        double[] ypr_deg = new double[3];
        m_imu.getYawPitchRoll(ypr_deg);
        SmartDashboard.putNumber("Robot IMU Yaw", ypr_deg[0]);
        return new Angle(ypr_deg[0] + m_offsetHeading_deg, AngleUnit.DEGREES).difference(new Angle());
    }


    /**
     * Used to get robot's current velocity relative to the field
     *
     * @return Robot Velocity relative to Field
     */
    public Vector2d getRobotVelocityVector() {
        Vector2d driveVelocityVector = m_drive.getVelocityVector();
        SmartDashboard.putNumber("F_Vx", driveVelocityVector.getX());
        SmartDashboard.putNumber("F_Vy", driveVelocityVector.getY());
        return driveVelocityVector.rotateSet(getRobotHeading().inverse());
    }

    /**
     * Used to get robot's current velocity relative to the Turret
     *
     * TODO: Will this method get used? Can it be deleted?
     *
     * @return Robot Velocity in Turret's Coordinate Frame
     */
    public Vector2d getTurretVelocityVector() {
        Vector2d robotVelocityVector = m_drive.getVelocityVector();
        return robotVelocityVector.rotateSet(m_turret.getAngle().inverse());
    }

    /**
     * Used to find the robot's vector to the target compensating for the offset between the turret and the drive
     */
    private void calculateCompensatedShotVector() {
        Vector2d shotVector = getShotVector();
        double shotVectorMagnitude_in = shotVector.magnitude();
        //Outer and inner goal heights are the same, so we just picked one
        double shotDistance_in = Math.sqrt(Math.pow(FieldDimensions.k_powerPortOuterHeight_in, 2.0) + Math.pow(shotVectorMagnitude_in, 2.0));
        double timeOfFlight_s = shotDistance_in / ShooterWheel.k_muzzleVelocity_inps;
        Vector2d compensateVector_in = getRobotVelocityVector().scale(timeOfFlight_s).inverseSet();
        m_compensatedShotVector = shotVector.add(compensateVector_in);
        SmartDashboard.putNumber("Comp Mag", m_compensatedShotVector.magnitude());
        SmartDashboard.putNumber("Comp Ang", m_compensatedShotVector.getAngle().getValue(AngleUnit.DEGREES));
    }

    /**
     * Gets the shot vector with velocity compensation included.
     *
     * @return Compensated vector to the shot target
     */
    public Vector2d getCompensatedShotVector(){
        return m_compensatedShotVector;
    }
}
