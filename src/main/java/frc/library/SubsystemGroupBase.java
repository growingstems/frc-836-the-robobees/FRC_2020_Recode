/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import java.util.HashSet;
import java.util.Set;

import edu.wpi.first.wpilibj2.command.Subsystem;

public class SubsystemGroupBase implements SubsystemGroup
{
    private Set<Subsystem> m_subsystems;

    public SubsystemGroupBase()
    {
        m_subsystems = new HashSet<Subsystem>();
    }

    public Set<Subsystem> getSubsystems()
    {
        return m_subsystems;
    }
    public void addSubsystem(Subsystem s)
    {
        m_subsystems.add(s);
    }
    public void removeSubsystem(Subsystem s)
    {
        m_subsystems.remove(s);
    }
    public void clearSubsystems()
    {
        m_subsystems.clear();
    }
    public void setSubsystems(Set<Subsystem> s)
    {
        m_subsystems = s;
    }
}