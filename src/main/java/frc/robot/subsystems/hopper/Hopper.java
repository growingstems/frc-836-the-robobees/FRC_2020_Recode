/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.hopper;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.library.Disableable;
import frc.robot.RobotMap;
import frc.robot.RobotState;

import java.util.function.DoubleSupplier;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.signals.Debounce;
import org.growingstems.util.statemachine.InitExitState;
import org.growingstems.util.statemachine.InitState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.statemachine.StateMachine;
import org.growingstems.util.statemachine.StaticTransition;

/**
 * Stores power cells from intake and feeds to shooter. <br>
 * <br>
 * Hopper uses a single motor to control belts. <br>
 * The hopper controls when the robot is able to shoot, as it controls
 * whether the turret has power cells. <br>
 * <br>
 * Issues include running while jammed and melting the belt.
 */
public class Hopper extends SubsystemBase implements Disableable {
    private static enum MotorPowerState {
        IDLE,
        PRIME,
        SHOOT,
        UNJAM
    }

    private final double k_primePower = 0.2;
    private final double k_shootPower = 0.8;
    private final double k_unjamPower = -0.5;
    private final double k_unjamCurrentThreshholdTime_s = 0.125;
    private final double k_unjamDuration_s = 0.5;
    private final double k_unjamCurrent_A = 21.0;

    private MotorPowerState m_hopperPower = MotorPowerState.IDLE;
    private boolean m_shouldShoot = false;
    private StateMachine m_stateMachine;
    private final VictorSPX m_hopperMotor = new VictorSPX(RobotMap.k_hopperMotorCanId);
    private final DigitalInput m_hopperPrime = new DigitalInput(RobotMap.k_dioHopperPrime);
    private final DigitalInput m_hopperInner = new DigitalInput(RobotMap.k_dioHopperInner);
    private final DigitalInput m_hopperOuter = new DigitalInput(RobotMap.k_dioHopperOuter);
    private final Debounce m_hasBallDebounce = new Debounce(0.2, new WpiTimer());
    private final Debounce m_isPrimedDebounce = new Debounce(0.2, new WpiTimer());
    private final Debounce m_unjamDebounce = new Debounce(k_unjamCurrentThreshholdTime_s, new WpiTimer());
    private final WpiTimer m_unjamTimer = new WpiTimer();
    private final DoubleSupplier m_hopperMotorCurrent_A;
    private final RobotState m_robotState;

    private final String standardModeStr = "standardMode";
    private final String unjamModeStr = "unjam";
    private final String idleStr = "idle";
    private final String primeStr = "prime";
    private final String shootStr = "shoot";

    /**
     * Creates a new Hopper subsystem.
     *
     * @param robotState A reference to the state of the robot.
     * @param hopperMotorCurrent_A A supplier to get the most recent current of the hopper motor.
     */
    public Hopper(RobotState robotState, DoubleSupplier hopperMotorCurrent_A) {
        m_robotState = robotState;
        m_hopperMotorCurrent_A = hopperMotorCurrent_A;
        State idleState = new InitState(idleStr, () -> m_hopperPower = MotorPowerState.IDLE);
        State primeState = new InitState(primeStr, () -> m_hopperPower = MotorPowerState.PRIME);
        State shootState = new InitState(shootStr, () -> m_hopperPower = MotorPowerState.SHOOT);
        StaticTransition toShoot = new StaticTransition(shootState, () -> m_shouldShoot && m_robotState.isReadyToShoot());

        idleState.addTransition(new StaticTransition(primeState, () -> hasBall() && !isPrimed()));
        idleState.addTransition(toShoot);
        primeState.addTransition(toShoot);
        primeState.addTransition(new StaticTransition(idleState, this::isPrimed));
        shootState.addTransition(new StaticTransition(idleState, () -> (!m_shouldShoot || !m_robotState.isReadyToShoot()) && !isPrimed()));

        State standardStateMachine = new StateMachine(idleState).getAsState(standardModeStr, false, true);
        State unjamState = new InitExitState(unjamModeStr, this::unjamInit, this::unjamExit);
        standardStateMachine.addTransition(new StaticTransition(unjamState,
            () -> m_unjamDebounce.update(m_hopperMotorCurrent_A.getAsDouble() > k_unjamCurrent_A)));
        unjamState.addTransition(new StaticTransition(standardStateMachine,
            () -> m_unjamTimer.hasElapsed(k_unjamDuration_s)));

        m_stateMachine = new StateMachine(standardStateMachine);
        m_stateMachine.start();
    }

    @Override
    public void periodic() {
        m_stateMachine.step();
        setSpeed(m_hopperPower);
    }

    private void setSpeed(MotorPowerState pow) {
        double speed = 0.0;
        switch(pow) {
            case IDLE:
                speed = 0.0;
                break;
            case PRIME:
                speed = k_primePower;
                break;
            case SHOOT:
                speed = k_shootPower;
                break;
            case UNJAM:
                speed = k_unjamPower;
                break;
            default:
                System.out.println("Hopper.setSpeed() has been called with an improper enum value: " + pow);
                break;
        }

        m_hopperMotor.set(ControlMode.PercentOutput, speed);
    }

    private boolean hasBall() {
        return m_hasBallDebounce.update(!m_hopperInner.get() || !m_hopperOuter.get());
    }

    private boolean isPrimed() {
        return m_isPrimedDebounce.update(!m_hopperPrime.get());
    }

    private void unjamInit() {
        m_unjamTimer.start();
        m_hopperPower = MotorPowerState.UNJAM;
    }

    private void unjamExit() {
        m_unjamTimer.stop();
        m_unjamTimer.reset();
        m_unjamDebounce.update(false);
    }

    /**
     * Sends a command to begin firing power cells.
     * @return the command to begin firing power cells.
     */
    public Command getFireCommand() {
        return new InstantCommand(() -> m_shouldShoot = true, this);
    }

    /**
     * Sends a command to stop firing power cells.
     * @return the command to stop firing power cells.
     */
    public Command getCeaseFireCommand() {
        return new InstantCommand(() -> m_shouldShoot = false, this);
    }

    /**
     * Returns whether the robot is in the shooting state or not.
     * @return whether the robot is in the shooting state or not.
     */
    public boolean isInShootState() {
        return m_stateMachine.getCurrentState().getStateName().equals(standardModeStr + "." + shootStr);
    }

    /**
     * Stops the state machine, then sets to hopper to idle.
     */
    @Override
    public void disable() {
        m_stateMachine.stop();
        setSpeed(MotorPowerState.IDLE);
        m_shouldShoot = false;
    }

    /**
     * Enables the hopper in an idle state. <br>
     * <br>
     * No-ops if the hopper is already running.
     */
    @Override
    public void enable() {
        if (m_stateMachine.isRunning()) {
            return;
        }
        disable();
        m_stateMachine.start();
    }
}
