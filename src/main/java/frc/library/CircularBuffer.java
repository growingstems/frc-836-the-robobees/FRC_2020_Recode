/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import java.util.ArrayDeque;
import java.util.Collection;

public class CircularBuffer<E> extends ArrayDeque<E> {
    private static final long serialVersionUID = 5457656889169613524L;
    private final int m_bufferSize;

    public CircularBuffer (int bufferSize) {
        m_bufferSize = bufferSize;
    }

    @Override
    public boolean add(E e) {
        super.add(e);
        shrink();
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        super.addAll(c);
        shrink();
        return true;
    }

    @Override
    public void addFirst(E e) {
        super.addFirst(e);
        shrink();
    }

    @Override
    public void addLast(E e) {
        super.addLast(e);
        shrink();
    }

    @Override
    public boolean offer(E e) {
        super.offer(e);
        shrink();
        return true;
    }

    @Override
    public boolean offerFirst(E e) {
        super.offerFirst(e);
        shrink();
        return true;
    }

    @Override
    public boolean offerLast(E e) {
        super.offerLast(e);
        shrink();
        return true;
    }

    @Override
    public void push(E e) {
        super.push(e);
        shrink();
    }

    private void shrink() {
        while (size() > m_bufferSize) {
            removeFirst();
        }
    }

    public int getSize() {
        return m_bufferSize;
    }
}
