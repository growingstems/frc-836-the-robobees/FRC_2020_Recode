/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.interpolation;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class InterpolatableDouble_Test {

    @Test
    @Parameters({
        "0.0, 0.0, 0.0, 0.0, Default test",
        "0.0, 10.0, 0.0, 0.0, 0 to 10 - 0.0 test",
        "0.0, 10.0, 0.1, 1.0, 0 to 10 - 0.1 test",
        "0.0, 10.0, 0.2, 2.0, 0 to 10 - 0.2 test",
        "0.0, 10.0, 0.3, 3.0, 0 to 10 - 0.3 test",
        "0.0, 10.0, 0.4, 4.0, 0 to 10 - 0.4 test",
        "0.0, 10.0, 0.5, 5.0, 0 to 10 - 0.5 test",
        "0.0, 10.0, 0.6, 6.0, 0 to 10 - 0.6 test",
        "0.0, 10.0, 0.7, 7.0, 0 to 10 - 0.7 test",
        "0.0, 10.0, 0.8, 8.0, 0 to 10 - 0.8 test",
        "0.0, 10.0, 0.9, 9.0, 0 to 10 - 0.9 test",
        "0.0, 10.0, 1.0, 10.0, 0 to 10 - 1.0 test",
        "0.0, 10.0, -0.5, -5.0, Negative extrapolate test",
        "0.0, 10.0, 2.0, 20.0, Positive extrapolate test",
        "10.0, 0.0, 0.0, 10.0, Reverse points - beginning",
        "10.0, 0.0, 0.5, 5.0, Reverse points - middle",
        "10.0, 0.0, 1.0, 0.0, Reverse points - end"
    })
    public void testInterpolate(double a, double b, double x, double expected, String testName) {
        InterpolatableDouble aObj = new InterpolatableDouble(a);
        InterpolatableDouble bObj = new InterpolatableDouble(b);

        InterpolatableDouble interpolatedValue = aObj.linearInterpolate(bObj, x);
        assertEquals(testName, expected, interpolatedValue.getValue(), 1E-9);
    }

    @Test
    @Parameters({
        "0.0, 10.0, 0.0, 0.0, 0 to 10 - 0.0 test",
        "0.0, 10.0, 1.0, 0.1, 0 to 10 - 1.0 test",
        "0.0, 10.0, 2.0, 0.2, 0 to 10 - 2.0 test",
        "0.0, 10.0, 3.0, 0.3, 0 to 10 - 3.0 test",
        "0.0, 10.0, 4.0, 0.4, 0 to 10 - 4.0 test",
        "0.0, 10.0, 5.0, 0.5, 0 to 10 - 5.0 test",
        "0.0, 10.0, 6.0, 0.6, 0 to 10 - 6.0 test",
        "0.0, 10.0, 7.0, 0.7, 0 to 10 - 7.0 test",
        "0.0, 10.0, 8.0, 0.8, 0 to 10 - 8.0 test",
        "0.0, 10.0, 9.0, 0.9, 0 to 10 - 9.0 test",
        "0.0, 10.0, 10.0, 1.0, 0 to 10 - 10.0 test",
        "0.0, 10.0, -5.0, -0.5, Negative extrapolate test",
        "0.0, 10.0, 20.0, 2.0, Positive extrapolate test",
        "10.0, 0.0, 10.0, 0.0, Reverse points - beginning",
        "10.0, 0.0, 5.0, 0.5, Reverse points - middle",
        "10.0, 0.0, 0.0, 1.0, Reverse points - end"
    })
    public void testInverseInterpolate(double a, double b, double other, double expected, String testName) {
        InterpolatableDouble aObj = new InterpolatableDouble(a);
        InterpolatableDouble bObj = new InterpolatableDouble(b);
        InterpolatableDouble otherObj = new InterpolatableDouble(other);

        double x = aObj.linearInverseInterpolate(bObj, otherObj);
        assertEquals(testName, expected, x, 1E-9);
    }

    @Test
    @Parameters({
        "0.0, 0.0, 0, Equal test",
        "1.0, 0.0, 1, Greater than test",
        "0.0, 1.0, -1, Less than test"
    })
    public void testComparable(double a, double b, int expectedResult, String testName) {
        InterpolatableDouble aObj = new InterpolatableDouble(a);
        InterpolatableDouble bObj = new InterpolatableDouble(b);

        assertEquals(testName, expectedResult, aObj.compareTo(bObj));
    }
}
