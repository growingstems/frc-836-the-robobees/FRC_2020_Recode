/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import java.util.Set;

import edu.wpi.first.wpilibj2.command.Subsystem;

public interface SubsystemGroup
{
    public Set<Subsystem> getSubsystems();
    public void addSubsystem(Subsystem s);
    public void removeSubsystem(Subsystem s);
    public void clearSubsystems();
    public void setSubsystems(Set<Subsystem> s);
}